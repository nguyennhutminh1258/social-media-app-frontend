import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { Button, Grid, IconButton, InputAdornment, Paper, Stack, TextField, Typography } from "@mui/material";
import { Box, Container } from "@mui/system";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { login } from "../../store/slice/userSlice";
import Register from "../register/Register";

const Login = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [openRegister, setOpenRegister] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  const token = useSelector((state) => state.user.token);
  const showAlert = useSelector((state) => state.alert.showAlert);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const handleOpenRegister = () => {
    setOpenRegister(true);
  };

  const handleCloseRegister = () => {
    setOpenRegister(false);
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleLogin = (data) => {
    if (!showAlert) {
      dispatch(login(data));
    }
  };

  useEffect(() => {
    if (token) navigate("/");
  }, [token, navigate]);

  return (
    <Box
      sx={{
        height: "100vh",
        width: "100vw",
        position: "relative",
        background:
          "linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(146,146,235,1) 0%, rgba(108,191,208,1) 100%)",
      }}>
      <Container
        maxWidth="sm"
        sx={{
          padding: 0,
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
        }}>
        <Grid container spacing={2} direction="column" justifyContent="center">
          <Paper elelvation={2} sx={{ padding: 5 }}>
            <form onSubmit={handleSubmit(handleLogin)}>
              <Typography
                component="h3"
                fontWeight="bold"
                fontSize="20px"
                mb="26px"
                letterSpacing="1px"
                lineHeight="16px"
                textAlign="center">
                Social Media App
              </Typography>
              <Grid container direction="column" spacing={2}>
                <Grid item>
                  <TextField
                    type="text"
                    fullWidth
                    label="Địa chỉ email"
                    variant="outlined"
                    {...register("email", {
                      required: "Vui lòng nhập email",
                      pattern: {
                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                        message: "Vui lòng nhập email hợp lệ",
                      },
                    })}
                    error={!!errors.email}
                    helperText={errors.email && errors.email.message}
                  />
                </Grid>

                <Grid item>
                  <TextField
                    type={showPassword ? "text" : "password"}
                    fullWidth
                    label="Mật khẩu"
                    variant="outlined"
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            onClick={handleClickShowPassword}
                            aria-label="toggle password visibility"
                            edge="end">
                            {showPassword ? <VisibilityOff /> : <Visibility />}
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                    {...register("password", { required: "Vui lòng nhập mật khẩu" })}
                    error={!!errors.password}
                    helperText={errors.password && errors.password.message}
                  />
                </Grid>

                <Grid item>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    disabled={!register}
                    sx={{ borderRadius: 20 }}>
                    Đăng nhập
                  </Button>
                </Grid>
                <Grid item>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <Typography component="p" fontWeight={500} color="primary">
                      Bạn chưa có tài khoản?
                    </Typography>
                    <Button
                      variant="text"
                      color="success"
                      sx={{ fontWeight: 700, borderRadius: 20, "&:hover": { textDecoration: "underline" } }}
                      onClick={handleOpenRegister}>
                      Đăng kí
                    </Button>
                  </Stack>
                </Grid>
              </Grid>
            </form>
          </Paper>
        </Grid>
      </Container>
      <Register open={openRegister} handleClose={handleCloseRegister} />
    </Box>
  );
};

export default Login;
