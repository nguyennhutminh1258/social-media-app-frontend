import {
  Avatar,
  Container,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListSubheader,
  Paper,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import FollowBtn from "../../components/follow-btn/FollowBtn";

const SuggestionUser = () => {
  const theme = useTheme();
  const minWidth750 = useMediaQuery("(min-width:750px)");
  const userSuggestion = useSelector((state) => state.suggestion.userSuggestion);

  return (
    <Container maxWidth="md" sx={{ marginTop: 2, marginBottom: `${minWidth750 ? "0" : "2"}` }}>
      <Paper
        sx={{
          height: `${minWidth750 ? "calc(100vh - 81px)" : "calc(100vh - 157px)"}`,
          backgroundColor: theme.palette.mode === "dark" ? "#121212" : "#f9f9f9",
          overflowY: "auto",
        }}>
        <Stack direction="row" spacing={2} my={2} justifyContent="center">
          <Stack width="100%" spacing={2}>
            <List
              subheader={
                userSuggestion?.length > 0 && (
                  <ListSubheader color="inherit" sx={{ background: "inherit", position: "relative" }}>
                    Đề xuất theo dõi
                  </ListSubheader>
                )
              }>
              {userSuggestion?.length === 0 && (
                <Stack alignItems="center">
                  <Typography component="span">Không có người dùng đề xuất.</Typography>
                </Stack>
              )}
              {userSuggestion?.map((suggestion, index) => {
                return (
                  <ListItem
                    key={suggestion._id}
                    secondaryAction={<FollowBtn userProfile={suggestion} />}
                    divider={userSuggestion?.length > 1 && index !== userSuggestion?.length - 1}>
                    <ListItemAvatar>
                      <Avatar src={suggestion.avatar} width={20} height={20} />
                    </ListItemAvatar>
                    <ListItemText
                      primary={
                        <Link
                          to={`/profile/${suggestion._id}`}
                          style={{ textDecoration: "none", color: "inherit" }}>
                          <Typography component="span" fontSize={13} fontWeight={600}>
                            {suggestion.userName}
                          </Typography>
                        </Link>
                      }
                      secondary={
                        <Typography component="span" fontSize={11} fontWeight={400}>
                          {suggestion.name}
                        </Typography>
                      }
                    />
                  </ListItem>
                );
              })}
            </List>
          </Stack>
        </Stack>
      </Paper>
    </Container>
  );
};

export default SuggestionUser;
