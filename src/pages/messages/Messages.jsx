import { Container, Paper, useMediaQuery, useTheme } from "@mui/material";
import { Stack } from "@mui/system";
import IconMessage from "../../components/icon-message/IconMessage";
import LeftMessages from "./LeftMessages";

const Messages = () => {
  const theme = useTheme();
  const minWidth750 = useMediaQuery("(min-width:750px)");

  return (
    <Container maxWidth="md" sx={{ marginTop: 2, marginBottom: `${minWidth750 ? "0" : "2"}` }}>
      <Paper
        sx={{
          height: `${minWidth750 ? "calc(100vh - 81px)" : "calc(100vh - 157px)"}`,
          backgroundColor: theme.palette.mode === "dark" ? "#121212" : "#f9f9f9",
        }}>
        <Stack direction="row" height="100%">
          <Stack
            width={"40%"}
            height="100%"
            sx={{
              borderRight: `1px solid ${
                theme.palette.mode === "dark" ? "rgb(54,54,54)" : "rgb(219,219,219)"
              }`,
            }}>
            <LeftMessages />
          </Stack>
          <Stack width={"60%"} height="100%">
            <Stack height="100%" width="100%" alignItems="center" justifyContent="center">
              <IconMessage />
            </Stack>
          </Stack>
        </Stack>
      </Paper>
    </Container>
  );
};

export default Messages;
