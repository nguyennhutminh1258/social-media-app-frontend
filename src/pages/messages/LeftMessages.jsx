import { List, Typography, useTheme } from "@mui/material";
import { Stack } from "@mui/system";
import { useEffect, useState } from "react";
import InputSearch from "../../components/search-box/InputSearch";
import Search from "../../components/search-box/Search";
import SearchIconWrapper from "../../components/search-box/SearchIconWrapper";
import SearchIcon from "@mui/icons-material/Search";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import {
  getConversations,
  searchUserMess,
  setFirstLoad,
  setUserListMess,
} from "../../store/slice/messageSlice";
import UserCard from "../../components/user-card/UserCard";

const LeftMessages = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const theme = useTheme();

  const [search, setSearch] = useState("");
  // Danh sách user tìm kiếm
  const searchList = useSelector((state) => state.message.searchList);
  // Danh sách user thêm vào phần trò chuyện
  const userList = useSelector((state) => state.message.userList);
  const firstLoad = useSelector((state) => state.message.firstLoad);
  // Thông tin user hiện tại
  const userInfo = useSelector((state) => state.user.userInfo);

  const handleOnSearch = (e) => {
    setSearch(e.target.value.toLowerCase().replace(/ /g, ""));
  };

  const handleAddUser = (user) => {
    if (!userList.includes(user)) {
      setSearch("");
      dispatch(setUserListMess(user));
      navigate(`/message/${user._id}`);
    }
  };

  useEffect(() => {
    // limit request to server
    const getData = setTimeout(() => {
      dispatch(searchUserMess(search));
    }, 500);

    return () => clearTimeout(getData);
  }, [dispatch, search]);

  useEffect(() => {
    if (firstLoad) {
      dispatch(getConversations(userInfo._id));
      dispatch(setFirstLoad(false));
    }
  }, [dispatch, firstLoad, userInfo._id]);

  return (
    <Stack>
      <Stack
        sx={{
          padding: 2,
          borderBottom: `1px solid ${
            theme.palette.mode === "dark" ? "rgb(54,54,54)" : "rgb(219,219,219)"
          }`,
        }}
      >
        <Typography
          component="span"
          textAlign="center"
          fontSize={18}
          fontWeight={700}
        >
          Tin nhắn
        </Typography>
      </Stack>
      <Stack
        sx={{
          padding: 1,
          borderBottom: `1px solid ${
            theme.palette.mode === "dark" ? "rgb(54,54,54)" : "rgb(219,219,219)"
          }`,
        }}
      >
        <Search>
          <SearchIconWrapper>
            <SearchIcon />
          </SearchIconWrapper>
          <InputSearch
            placeholder="Tìm kiếm..."
            inputProps={{ "aria-label": "search" }}
            value={search}
            onChange={(e) => handleOnSearch(e)}
          />
        </Search>
      </Stack>
      <Stack sx={{ paddingBottom: 1, paddingTop: 1 }}>
        {searchList.length > 0 ? (
          <List
            sx={{
              borderRadius: 2,
              width: "100%",
            }}
          >
            {searchList
              ?.filter((item) => item._id !== userInfo._id)
              .map((user) => {
                return (
                  <Stack
                    key={user._id}
                    onClick={() => handleAddUser(user)}
                    sx={{
                      background: `${
                        id === user._id
                          ? theme.palette.mode === "dark"
                            ? "#454545"
                            : "#f0f0f0"
                          : "transparent"
                      }`,
                    }}
                  >
                    <UserCard user={user} handleAddUser={handleAddUser} />
                  </Stack>
                );
              })}
          </List>
        ) : (
          <List
            sx={{
              borderRadius: 2,
              width: "100%",
            }}
          >
            {userList?.map((user) => {
              return (
                <Stack
                  key={user._id}
                  onClick={() => navigate(`/message/${user._id}`)}
                  sx={{
                    background: `${
                      id === user._id
                        ? theme.palette.mode === "dark"
                          ? "#454545"
                          : "#f0f0f0"
                        : "transparent"
                    }`,
                  }}
                >
                  <UserCard user={user} hasMessage={true} />
                </Stack>
              );
            })}
          </List>
        )}
      </Stack>
    </Stack>
  );
};

export default LeftMessages;
