import { CircularProgress, Container, Stack, useMediaQuery } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import StatusModal from "../../components/status-modal/StatusModal";
import Status from "../../components/status/Status";
import { getPosts } from "../../store/slice/postSlice";
import { lazy, Suspense, useCallback, useEffect } from "react";
import SuggestionUser from "../suggestion-user/SuggestionUser";

const Posts = lazy(() => import("../../components/posts/Posts"));

const Home = () => {
  const dispatch = useDispatch();
  const minWidth750 = useMediaQuery("(min-width:750px)");

  const userInfo = useSelector((state) => state.user.userInfo);
  const hasMore = useSelector((state) => state.post.hasMore);
  const loadingPost = useSelector((state) => state.post.loadingPost);
  const posts = useSelector((state) => state.post.posts);

  const handleScroll = useCallback(() => {
    if (
      window.innerHeight + document.documentElement.scrollTop > document.documentElement.offsetHeight - 60 &&
      hasMore
    ) {
      dispatch(getPosts(userInfo._id));
    }
  }, [dispatch, hasMore, userInfo._id]);

  useEffect(() => {
    if (!loadingPost) {
      window.addEventListener("scroll", handleScroll);
    }
    return () => window.removeEventListener("scroll", handleScroll);
  }, [handleScroll, loadingPost]);

  return (
    <>
      {posts?.length === 0 ? (
        <SuggestionUser />
      ) : (
        <>
          <Container maxWidth="md" sx={{ marginBottom: minWidth750 ? 0 : 8 }}>
            <Stack direction="row" spacing={2} my={2} justifyContent="center">
              <Stack width="100%" spacing={2}>
                <Status />
                <Suspense
                  fallback={
                    <Stack justifyContent="center" alignItems="center" sx={{ marginTop: 2 }}>
                      <CircularProgress />
                    </Stack>
                  }>
                  <Posts />
                </Suspense>
                {loadingPost && (
                  <Stack justifyContent="center" alignItems="center" sx={{ marginTop: 2 }}>
                    <CircularProgress />
                  </Stack>
                )}
              </Stack>
            </Stack>
          </Container>
          <StatusModal />
        </>
      )}
    </>
  );
};

export default Home;
