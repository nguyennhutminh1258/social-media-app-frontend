import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  IconButton,
  InputAdornment,
  InputLabel,
  Radio,
  RadioGroup,
  Stack,
  TextField,
} from "@mui/material";
import { useRef, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { registerUser } from "../../store/slice/userSlice";

const Register = ({ open, handleClose }) => {
  const dispatch = useDispatch();

  const [showPassword, setShowPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const {
    register,
    handleSubmit,
    control,
    reset,
    watch,
    formState: { errors },
  } = useForm({
    defaultValues: {
      gender: "male",
    },
  });

  const password = useRef({});
  password.current = watch("password", "");

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleClickShowConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  const handleRegister = (data) => {
    dispatch(registerUser(data));
  };

  return (
    <Dialog fullWidth={true} maxWidth="md" open={open} onClose={handleClose}>
      <form onSubmit={handleSubmit(handleRegister)} style={{ padding: "20px 0" }}>
        <DialogTitle
          fontWeight="bold"
          fontSize="20px"
          letterSpacing="0.5px"
          lineHeight="16px"
          textAlign="center">
          Đăng kí tài khoản
        </DialogTitle>
        <DialogContent>
          <Stack spacing={3} mt={2}>
            <TextField
              label="Họ và tên"
              type="text"
              fullWidth
              variant="outlined"
              {...register("name", { required: "Vui lòng nhập họ và tên" })}
              error={!!errors.name}
              helperText={errors.name && errors.name.message}
            />
            <TextField
              label="Tên tài khoản"
              type="text"
              fullWidth
              variant="outlined"
              {...register("userName", {
                required: "Vui lòng nhập tên tài khoản",
                pattern: {
                  value: /^[a-z\d_]{4,20}$/,
                  message: "Vui lòng nhập username hợp lệ không chữ viết hoa và dấu cách",
                },
              })}
              error={!!errors.userName}
              helperText={errors.userName && errors.userName.message}
            />
            <TextField
              label="Địa chỉ email"
              type="text"
              fullWidth
              variant="outlined"
              {...register("email", {
                required: "Vui lòng nhập email",
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                  message: "Vui lòng nhập email hợp lệ",
                },
              })}
              error={!!errors.email}
              helperText={errors.email && errors.email.message}
            />
            <TextField
              type={showPassword ? "text" : "password"}
              fullWidth
              label="Mật khẩu"
              variant="outlined"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={handleClickShowPassword}
                      aria-label="toggle password visibility"
                      edge="end">
                      {showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              {...register("password", {
                required: "Vui lòng nhập mật khẩu",
                minLength: {
                  value: 6,
                  message: "Mật khẩu phải chứa ít nhất 6 kí tự",
                },
              })}
              error={!!errors.password}
              helperText={errors.password && errors.password.message}
            />
            <TextField
              type={showConfirmPassword ? "text" : "password"}
              fullWidth
              label="Nhập lại mật khẩu"
              variant="outlined"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      onClick={handleClickShowConfirmPassword}
                      aria-label="toggle password visibility"
                      edge="end">
                      {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              {...register("confirmPassword", {
                validate: (value) =>
                  value === password.current || "Mật khẩu không khớp nhau vui lòng nhập lại",
              })}
              error={!!errors.confirmPassword}
              helperText={errors.confirmPassword && errors.confirmPassword.message}
            />
            <Controller
              control={control}
              name="gender"
              render={({ field: { onChange, value } }) => (
                <>
                  <InputLabel>Giới tính</InputLabel>
                  <RadioGroup row value={value} onChange={onChange} style={{ marginTop: "0px" }}>
                    <FormControlLabel value="male" control={<Radio />} label="Nam" />
                    <FormControlLabel value="female" control={<Radio />} label="Nữ" />
                    <FormControlLabel value="other" control={<Radio />} label="Khác" />
                  </RadioGroup>
                </>
              )}
            />
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              handleClose();
              reset();
            }}
            variant="contained"
            color="warning"
            sx={{ borderRadius: 20 }}>
            Hủy
          </Button>
          <Button type="submit" variant="contained" color="success" sx={{ borderRadius: 20 }}>
            Đăng kí
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  );
};

export default Register;
