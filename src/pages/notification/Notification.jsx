import {
  Avatar,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  ListSubheader,
  Menu,
  Typography,
  Stack,
  MenuItem,
  IconButton,
  Container,
  useMediaQuery,
  Paper,
  useTheme,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";
import { Link } from "react-router-dom";
import { deleteAllNotify, readNotify } from "../../store/slice/notifySlice";
import { useState } from "react";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import DeleteIcon from "@mui/icons-material/Delete";

const Notification = () => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const minWidth750 = useMediaQuery("(min-width:750px)");

  const notifies = useSelector((state) => state.notify);
  const userInfo = useSelector((state) => state.user.userInfo);

  // Hiện menu option
  const [anchorEl, setAnchorEl] = useState(null);
  const menuNotifyId = "menu-option-notify";
  const isMenuOpen = Boolean(anchorEl);

  const handleMenuNotifyOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleClickDeleteNotify = () => {
    if (notifies?.length > 0) {
      dispatch(deleteAllNotify(userInfo._id));
    }
    handleMenuClose();
  };

  const handleClickReadNotify = (id) => {
    dispatch(readNotify(id));
  };

  return (
    <>
      <Container maxWidth="md" sx={{ marginTop: 2, marginBottom: `${minWidth750 ? "0" : "2"}` }}>
        <Paper
          sx={{
            height: `${minWidth750 ? "calc(100vh - 81px)" : "calc(100vh - 157px)"}`,
            backgroundColor: theme.palette.mode === "dark" ? "#121212" : "#f9f9f9",
            overflowY: "auto",
          }}>
          <Stack direction="row" spacing={2} my={2} justifyContent="center">
            <Stack width="100%" spacing={2}>
              <List
                subheader={
                  <ListSubheader color="inherit" sx={{ background: "inherit", position: "relative" }}>
                    <Stack direction="row" alignItems="center" justifyContent="space-between">
                      <Typography component="span" fontSize={16} fontWeight={500}>
                        Thông báo
                      </Typography>
                      <IconButton onClick={handleMenuNotifyOpen}>
                        <MoreHorizIcon />
                      </IconButton>
                    </Stack>
                  </ListSubheader>
                }>
                {notifies?.length === 0 && (
                  <Stack alignItems="center">
                    <Typography component="span">Bạn không có thông báo nào.</Typography>
                  </Stack>
                )}
                {notifies?.map((notify, index) => {
                  return (
                    <ListItem
                      alignItems="flex-start"
                      divider={notifies?.length > 1 && index !== notifies?.length - 1}
                      key={notify._id}
                      onClick={() => handleClickReadNotify(notify._id)}>
                      <Link
                        to={`${notify.url}`}
                        style={{ width: "100%", textDecoration: "none", color: "inherit" }}>
                        <Stack width="100%" direction="row" justifyContent="space-between">
                          <Stack direction="row">
                            <ListItemAvatar>
                              <Avatar alt="avatar-user" src={notify.user?.avatar} />
                            </ListItemAvatar>
                            <Stack direction="column">
                              <ListItemText
                                primary={
                                  <Typography component="span">
                                    <Typography component="span" sx={{ fontWeight: 600, fontSize: 13 }}>
                                      {notify.user?.userName}
                                    </Typography>{" "}
                                    <Typography component="span" sx={{ fontWeight: 400, fontSize: 13 }}>
                                      {notify?.text}
                                    </Typography>
                                  </Typography>
                                }
                                secondary={
                                  <Typography
                                    component="span"
                                    variant="body2"
                                    color="text.primary"
                                    sx={{ display: "block", fontWeight: 400, fontSize: 13 }}>
                                    {notify.content
                                      ? notify?.content.length < 20
                                        ? notify?.content
                                        : notify?.content?.slice(0, 20) + "..."
                                      : ""}
                                  </Typography>
                                }
                              />
                              <Typography
                                sx={{ display: "block", fontWeight: 400, fontSize: 12 }}
                                component="span"
                                variant="body2"
                                color="text.primary">
                                {moment(notify?.createdAt).fromNow()}
                              </Typography>
                            </Stack>
                          </Stack>
                          {!notify.isRead && (
                            <Stack justifyContent="center">
                              <FiberManualRecordIcon
                                size="small"
                                sx={{ height: 12, width: 12, color: "#3ea6ff" }}
                              />
                            </Stack>
                          )}
                        </Stack>
                      </Link>
                    </ListItem>
                  );
                })}
              </List>
            </Stack>
          </Stack>
        </Paper>
      </Container>
      <Menu anchorEl={anchorEl} id={menuNotifyId} open={isMenuOpen} onClose={handleMenuClose}>
        <MenuItem onClick={handleClickDeleteNotify}>
          <DeleteIcon sx={{ marginRight: 1 }} size="small" />
          <Typography component="span" fontSize={14} fontWeight={400}>
            Xóa tất cả thông báo
          </Typography>
        </MenuItem>
      </Menu>
    </>
  );
};

export default Notification;
