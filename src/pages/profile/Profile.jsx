import {
  Box,
  Button,
  CircularProgress,
  Container,
  LinearProgress,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import InfoUser from "../../components/info-user/InfoUser";
import PostCard from "../../components/post-card/PostCard";
import StatusModal from "../../components/status-modal/StatusModal";
import {
  getUserPost,
  getUserProfile,
  setEmptyPost,
} from "../../store/slice/profileSlice";
import { setShowStatus } from "../../store/slice/statusSlice";

const Profile = () => {
  const { id } = useParams();
  const dispatch = useDispatch();

  const minWidth750 = useMediaQuery("(min-width:750px)");
  const minWidth1175 = useMediaQuery("(min-width:1175px)");

  const userInfo = useSelector((state) => state.user.userInfo);
  const userProfile = useSelector((state) => state.profile.userProfile);
  const postUserProfile = useSelector((state) => state.profile.posts);
  const hasMore = useSelector((state) => state.profile.hasMore);
  const loadingPost = useSelector((state) => state.profile.loadingPost);
  // const skip = useSelector((state) => state.profile.skip);

  const handleScroll = useCallback(() => {
    if (
      window.innerHeight + document.documentElement.scrollTop >
        document.documentElement.offsetHeight - 60 &&
      hasMore
    ) {
      dispatch(getUserPost(id));
    }
  }, [dispatch, hasMore, id]);

  useEffect(() => {
    if (!loadingPost) {
      window.addEventListener("scroll", handleScroll);
    }
    return () => window.removeEventListener("scroll", handleScroll);
  }, [handleScroll, loadingPost]);

  useEffect(() => {
    if (id !== userProfile?._id) {
      dispatch(setEmptyPost());
      dispatch(getUserPost(id));
      dispatch(getUserProfile(id));
    }
  }, [dispatch, id, userProfile?._id]);

  return (
    <>
      {id !== userProfile?._id ? (
        <Stack
          sx={{ width: minWidth1175 ? "80vw" : minWidth750 ? "90vw" : "100vw" }}
        >
          <LinearProgress />
        </Stack>
      ) : (
        <Container maxWidth="md" sx={{ marginBottom: minWidth750 ? 0 : 8 }}>
          <Box sx={{ marginBottom: 2 }}>
            <InfoUser />
          </Box>
          {postUserProfile?.length === 0 &&
          userProfile?._id === userInfo._id ? (
            <Stack direction="row" alignItems="center" justifyContent="center">
              <Typography component="span">
                <Typography component="span" fontSize={14} fontWeight={500}>
                  Bạn chưa có bài viết nào. Tạo bài viết đầu tiên
                </Typography>
                <Button
                  component="span"
                  variant="text"
                  sx={{
                    fontSize: 14,
                    textTransform: "none",
                    "&:hover": {
                      backgroundColor: "transparent",
                      textDecoration: "underline",
                    },
                  }}
                  disableRipple
                  onClick={() => dispatch(setShowStatus(true))}
                >
                  Tại đây
                </Button>
              </Typography>
              <StatusModal />
            </Stack>
          ) : (
            <Stack spacing={2} alignItems="center">
              {postUserProfile?.map((post) => {
                return <PostCard post={post} key={post._id} />;
              })}
            </Stack>
          )}
          {loadingPost && (
            <Stack
              justifyContent="center"
              alignItems="center"
              sx={{ marginTop: 2 }}
            >
              <CircularProgress />
            </Stack>
          )}
        </Container>
      )}
      <StatusModal />
    </>
  );
};

export default Profile;
