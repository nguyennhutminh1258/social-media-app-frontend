import { useTheme } from "@emotion/react";
import AttachFileIcon from "@mui/icons-material/AttachFile";
import CloseIcon from "@mui/icons-material/Close";
import DeleteIcon from "@mui/icons-material/Delete";
import SendOutlinedIcon from "@mui/icons-material/SendOutlined";
import SentimentVerySatisfiedIcon from "@mui/icons-material/SentimentVerySatisfied";
import {
  Box,
  CircularProgress,
  Container,
  IconButton,
  ImageList,
  ImageListItem,
  InputBase,
  Paper,
  Stack,
  useMediaQuery,
} from "@mui/material";
import { useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import EmojiMenu from "../../components/emoji-menu/EmojiMenu";
import Message from "../../components/message/Message";
import UserCard from "../../components/user-card/UserCard";
import { setMessageError } from "../../store/slice/alertSlice";
import { addMessage, getMessages } from "../../store/slice/messageSlice";
import { imageUpload } from "../../utils/imageUpload";
import LeftMessages from "../messages/LeftMessages";

const Conversation = () => {
  const { id } = useParams();
  const theme = useTheme();
  const dispatch = useDispatch();
  const minWidth750 = useMediaQuery("(min-width:750px)");

  const [currentUser, setCurrentUser] = useState();

  const userList = useSelector((state) => state.message.userList);
  const data = useSelector((state) => state.message.data);
  const loadingConversation = useSelector(
    (state) => state.message.loadingConversation
  );
  const userInfo = useSelector((state) => state.user.userInfo);

  const { register, handleSubmit, reset, setValue, getValues } = useForm();

  // Danh sách hình ảnh cho gửi qua tin nhắn
  const [images, setImages] = useState([]);
  // Hiện menu emoji
  const [anchorEmoji, setAnchorEmoji] = useState(null);
  const openEmoji = Boolean(anchorEmoji);

  const messagesEndRef = useRef(null);

  const handleOpenEmoji = (event) => {
    setAnchorEmoji(event.currentTarget);
  };
  const handleCloseEmoji = () => {
    setAnchorEmoji(null);
  };

  const handleAttachFile = (e) => {
    // Chọn ảnh từ thiết bị
    const files = [...e.target.files];

    let newImages = [];
    files.forEach((file) => {
      if (!file) {
        return dispatch(setMessageError("File không tồn tại."));
      } else if (file.type !== "image/jpeg" && file.type !== "image/png") {
        return dispatch(setMessageError("Định dạng file không đúng"));
      } else {
        return newImages.push(file);
      }
    });

    setImages([...images, ...newImages]);
  };

  const removeImage = (index) => {
    const newImages = [...images];
    newImages.splice(index, 1);
    setImages(newImages);
  };

  const handleSendMessage = async (data) => {
    let media = [];
    if (images.length > 0) media = await imageUpload(images);
    const newMessage = {
      sender: userInfo._id,
      recipient: id,
      text: data.content,
      media,
      createdAt: new Date().toISOString(),
    };
    dispatch(addMessage(newMessage));
    setImages([]);
    reset();
  };

  useEffect(() => {
    const newUser = userList.find((user) => user._id === id);

    if (newUser) {
      setCurrentUser(newUser);
    }
  }, [id, userList]);

  useEffect(() => {
    if (id) {
      dispatch(getMessages(id));
    }
  }, [dispatch, id]);

  useEffect(() => {
    if (!loadingConversation || data) {
      messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
    }
  }, [data, loadingConversation]);

  return (
    <>
      <Container
        maxWidth="md"
        sx={{ marginTop: 2, marginBottom: `${minWidth750 ? "0" : "2"}` }}
      >
        <Paper
          sx={{
            height: `${
              minWidth750 ? "calc(100vh - 81px)" : "calc(100vh - 157px)"
            }`,
            backgroundColor:
              theme.palette.mode === "dark" ? "#121212" : "#f9f9f9",
          }}
        >
          <Stack direction="row" height="100%">
            <Stack
              width={"35%"}
              height="100%"
              sx={{
                borderRight: `1px solid ${
                  theme.palette.mode === "dark"
                    ? "rgb(54,54,54)"
                    : "rgb(219,219,219)"
                }`,
              }}
            >
              <LeftMessages />
            </Stack>
            <Stack width={"65%"} height="100%">
              <Stack height="100%" width="100%">
                <Stack
                  direction="row"
                  alignItems="center"
                  sx={{
                    borderBottom: `1px solid ${
                      theme.palette.mode === "dark"
                        ? "rgb(54,54,54)"
                        : "rgb(219,219,219)"
                    }`,
                  }}
                >
                  <UserCard user={currentUser} notDevider={true} />
                  <IconButton sx={{ padding: 1, marginRight: 2 }}>
                    <DeleteIcon />
                  </IconButton>
                </Stack>
                {loadingConversation ? (
                  <Stack
                    justifyContent={"center"}
                    alignItems={"center"}
                    sx={{
                      width: "100%",
                      height: "calc(100% - 110px)",
                      overflowY: "auto",
                      borderBottom: `1px solid ${
                        theme.palette.mode === "dark"
                          ? "rgb(54,54,54)"
                          : "rgb(219,219,219)"
                      }`,
                    }}
                  >
                    <CircularProgress color="inherit" />
                  </Stack>
                ) : (
                  <Box
                    sx={{
                      width: "100%",
                      height: "calc(100% - 110px)",
                      overflowY: "auto",
                      borderBottom: `1px solid ${
                        theme.palette.mode === "dark"
                          ? "rgb(54,54,54)"
                          : "rgb(219,219,219)"
                      }`,
                    }}
                  >
                    <Stack
                      direction="column"
                      justifyContent="flex-end"
                      spacing={2}
                      sx={{
                        padding: 2,
                        minHeight: "100%",
                        overflowY: "auto",
                      }}
                    >
                      {data.map((message, index) => {
                        return (
                          <Message
                            key={index + 100}
                            user={
                              message.sender !== userInfo._id
                                ? currentUser
                                : userInfo
                            }
                            message={message}
                            owner={message.sender === userInfo._id}
                          />
                        );
                      })}
                    </Stack>
                    <Box ref={messagesEndRef} />
                  </Box>
                )}
                {images.length > 0 && (
                  <Stack sx={{ marginTop: 1 }}>
                    <ImageList sx={{ width: "100%", height: 100 }} cols={5}>
                      {images.map((item, index) => (
                        <ImageListItem
                          key={index}
                          sx={{ width: 75, height: 75 }}
                        >
                          <img
                            style={{ width: "100%", height: "100%" }}
                            cols={1}
                            rows={1}
                            src={
                              item.camera
                                ? item.camera
                                : item.url
                                ? item.url
                                : URL.createObjectURL(item)
                            }
                            alt="images"
                            sx={{ objectFit: "contain" }}
                          />
                          <IconButton
                            aria-label="close"
                            onClick={() => removeImage(index)}
                            sx={{
                              position: "absolute",
                              right: 0,
                              top: 0,
                              color: (theme) => theme.palette.grey[900],
                            }}
                          >
                            <CloseIcon />
                          </IconButton>
                        </ImageListItem>
                      ))}
                    </ImageList>
                  </Stack>
                )}
                <Stack
                  width="100%"
                  direction="row"
                  justifyContent="space-between"
                  sx={{ padding: 2 }}
                >
                  <form
                    onSubmit={handleSubmit(handleSendMessage)}
                    style={{ width: "100%" }}
                  >
                    <Stack width="100%" direction="row" alignItems="center">
                      <InputBase
                        {...register("content", {
                          required: true,
                        })}
                        multiline={true}
                        size="small"
                        fullWidth
                        placeholder="Nhắn tin..."
                        sx={{
                          color: "inherit",
                          width: "100%",
                          "& .MuiInputBase-input": {
                            padding: theme.spacing(1, 1, 1, 1.5),
                            width: "100%",
                            fontSize: "13px",
                            "&::placeholder": {
                              fontSize: "13px",
                            },
                          },
                        }}
                      />
                      <IconButton aria-label="upload picture" component="label">
                        <input
                          hidden
                          multiple
                          accept="image/*"
                          type="file"
                          onChange={(e) => handleAttachFile(e)}
                        />
                        <AttachFileIcon />
                      </IconButton>
                      <IconButton onClick={handleOpenEmoji}>
                        <SentimentVerySatisfiedIcon />
                      </IconButton>
                      <IconButton type="submit">
                        <SendOutlinedIcon />
                      </IconButton>
                    </Stack>
                  </form>
                </Stack>
              </Stack>
            </Stack>
          </Stack>
        </Paper>
      </Container>
      <EmojiMenu
        anchorEmoji={anchorEmoji}
        openEmoji={openEmoji}
        handleCloseEmoji={handleCloseEmoji}
        setValue={setValue}
        getValues={getValues}
      />
    </>
  );
};

export default Conversation;
