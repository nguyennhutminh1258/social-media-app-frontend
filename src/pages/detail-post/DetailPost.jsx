import { Stack } from "@mui/system";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate, useParams } from "react-router-dom";
import ImageCarosel from "../../components/image-carosel/ImageCarosel";
import { getDetailPost, setEmptyDetailPost } from "../../store/slice/detailPostSlice";
import CancelIcon from "@mui/icons-material/Cancel";
import { Avatar, Box, Divider, IconButton, Menu, MenuItem, Typography, useTheme } from "@mui/material";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import { setPostEdit } from "../../store/slice/statusSlice";
import { deletePost, likePost, setOnEdit, unLikePost } from "../../store/slice/postSlice";
import moment from "moment";
import StatusModal from "../../components/status-modal/StatusModal";
import LikeButton from "../../components/like-button/LikeButton";
import ModeCommentOutlinedIcon from "@mui/icons-material/ModeCommentOutlined";
import InputComment from "../../components/input-comment/InputComment";
import Comments from "../../components/comments/Comments";
import ConfirmDialog from "../../components/confirm-dialog/ConfirmDialog";
import { deleteUserPost } from "../../store/slice/profileSlice";

const DetailPost = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const theme = useTheme();

  const detailPost = useSelector((state) => state.detailPost);
  const userInfo = useSelector((state) => state.user.userInfo);
  const token = useSelector((state) => state.user.token);
  const socket = useSelector((state) => state.socket);

  const { images } = detailPost;

  // Hiện menu sửa, xóa bài viết
  const [anchorElMenu, setAnchorElMenu] = useState(null);
  const open = Boolean(anchorElMenu);
  // Thích bài viết
  const [liked, setLiked] = useState(false);
  // Hiện thêm / Ẩn bớt nội dung status
  const [readMore, setReadMore] = useState(false);
  // Hiện dialog xác nhận xóa bài viết
  const [openConfirm, setOpenConfirm] = useState(false);

  const handleOpenConfirm = () => {
    setOpenConfirm(true);
  };

  const handleCloseConfirm = () => {
    setOpenConfirm(false);
  };

  const handleReadMore = () => {
    setReadMore(!readMore);
  };

  const handleClick = (event) => {
    setAnchorElMenu(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorElMenu(null);
  };

  const handleEditPost = () => {
    dispatch(setPostEdit(detailPost));
    dispatch(setOnEdit(true));
    handleClose();
  };

  const handleLikePost = () => {
    // Kiểm tra user đã đăng nhập chưa
    if (token) {
      setLiked(true);
      dispatch(likePost({ post: detailPost, userInfo, socket }));
    } else {
      navigate("/login");
    }
  };

  const handleUnlikePost = () => {
    setLiked(false);
    dispatch(unLikePost({ post: detailPost, userInfo, socket }));
  };

  const handleDeletePost = () => {
    dispatch(deletePost(detailPost));
    dispatch(deleteUserPost(detailPost));
    handleCloseConfirm();
    handleClose();
    navigate(-1);
  };

  const handleCopyLink = () => {
    navigator.clipboard.writeText(`http://localhost:3000/post/${detailPost._id}`);
    handleClose();
  };

  useEffect(() => {
    dispatch(setEmptyDetailPost());
    dispatch(getDetailPost(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (detailPost.likes?.find((like) => like._id === userInfo._id)) {
      setLiked(true);
    }
  }, [detailPost.likes, userInfo._id]);

  return (
    <>
      <Stack position="relative" direction={{ md: "column", lg: "row" }} height="100vh">
        <Stack
          position="relative"
          alignItems="center"
          justifyContent="center"
          width={{ md: "100%", lg: "75%" }}
          height="100%"
          sx={{ backgroundColor: "theme.palette.background.default" }}>
          <IconButton
            sx={{ position: "absolute", top: 10, left: 10, color: theme.palette.text.primary, zIndex: 100 }}
            onClick={() => {
              navigate(-1);
              dispatch(setEmptyDetailPost());
            }}>
            <CancelIcon sx={{ width: 35, height: 35 }} />
          </IconButton>
          <ImageCarosel images={images} detail={true} />
        </Stack>
        <Stack
          width={{ md: "100%", lg: "25%" }}
          sx={{ padding: 2, backgroundColor: theme.palette.mode === "dark" ? "#212121" : "#f9f9f9" }}
          spacing={3}>
          <Stack direction="row" alignItems="center" justifyContent="space-between">
            <Stack direction="row" spacing={2} alignItems="center">
              <Stack>
                <Avatar src={detailPost.user?.avatar} />
              </Stack>
              <Stack>
                <Typography component="h4" fontWeight={700}>
                  <Link
                    to={`/profile/${detailPost.user?._id}`}
                    style={{ textDecoration: "none", color: "inherit" }}>
                    {detailPost.user?.userName}
                  </Link>
                </Typography>
                <Typography component="span" fontSize={13} fontWeight={400}>
                  {moment(detailPost.createdAt).fromNow()}
                </Typography>
              </Stack>
            </Stack>
            <Stack>
              <IconButton aria-label="settings" onClick={handleClick}>
                <MoreVertIcon />
              </IconButton>
            </Stack>
          </Stack>
          <Stack>
            <Typography variant="body2" color="text.secondary" fontWeight={500} whiteSpace="pre-line">
              {detailPost.content?.length < 60
                ? detailPost?.content
                : readMore
                ? detailPost?.content + " "
                : detailPost.content?.slice(0, 60) + "..."}{" "}
              {detailPost.content?.length > 60 && (
                <Typography
                  component="span"
                  fontSize={15}
                  fontWeight={700}
                  sx={{
                    cursor: "pointer",
                    "&:hover": {
                      textDecoration: "underline",
                    },
                  }}
                  onClick={handleReadMore}>
                  {readMore ? "Ẩn bớt" : "Xem thêm"}
                </Typography>
              )}
            </Typography>
          </Stack>
          <Divider sx={{ margin: "10px 0" }} />
          <Stack sx={{ width: "100%" }}>
            <Stack direction="row">
              <LikeButton liked={liked} handleLike={handleLikePost} handleUnlike={handleUnlikePost} />
              <IconButton aria-label="comment" size="large">
                <ModeCommentOutlinedIcon />
              </IconButton>
            </Stack>
            <Stack direction="row" justifyContent="space-between" px={2}>
              <Typography component="span" sx={{ fontSize: 14, fontWeight: 700, lineHeight: 1 }}>
                {detailPost.likes?.length} lượt thích
              </Typography>
              <Typography component="span" sx={{ fontSize: 14, fontWeight: 700, lineHeight: 1 }}>
                {detailPost.comments?.length} bình luận
              </Typography>
            </Stack>
          </Stack>
          <Divider sx={{ margin: "10px 0" }} />
          <Stack sx={{ width: "100%", maxHeight: { md: "20vh", lg: "70vh" }, overflowY: "auto" }}>
            {token && <InputComment sx={{ paddingLeft: "10px" }} post={detailPost} />}
            {token && <Divider sx={{ margin: "10px 0" }} />}
            <Comments post={detailPost} />
          </Stack>
        </Stack>
      </Stack>
      <Menu
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        anchorEl={anchorElMenu}
        open={open}
        onClose={handleClose}>
        {userInfo?._id === detailPost.user?._id ? (
          <Box>
            <MenuItem onClick={handleEditPost} disableRipple>
              <EditIcon sx={{ marginRight: 1.5 }} fontSize="small" />
              <Typography component="span" fontSize={15}>
                Sửa bài viết
              </Typography>
            </MenuItem>
            <Divider sx={{ my: 0.5 }} />
            <MenuItem onClick={handleOpenConfirm} disableRipple>
              <DeleteIcon sx={{ marginRight: 1.5 }} fontSize="small" />
              <Typography component="span" fontSize={15}>
                Xóa bài viết
              </Typography>
            </MenuItem>
          </Box>
        ) : (
          <MenuItem onClick={handleCopyLink} disableRipple>
            <ContentCopyIcon sx={{ marginRight: 1.5 }} fontSize="small" />
            <Typography component="span" fontSize={15}>
              Sao chép liên kết
            </Typography>
          </MenuItem>
        )}
      </Menu>
      <StatusModal />
      <ConfirmDialog
        openConfirm={openConfirm}
        handleCloseConfirm={handleCloseConfirm}
        handleDeletePost={handleDeletePost}
      />
    </>
  );
};

export default DetailPost;
