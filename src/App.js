import {
  Alert,
  Backdrop,
  CircularProgress,
  Paper,
  Snackbar,
  CssBaseline,
  Stack,
  LinearProgress,
  useMediaQuery,
} from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter as Router, Navigate, Outlet, Route, Routes } from "react-router-dom";
import { setMessageError, setMessageSuccess } from "./store/slice/alertSlice";
import { getPosts } from "./store/slice/postSlice";
import { getUserSuggestion } from "./store/slice/suggestionSlice";
import { refreshToken } from "./store/slice/userSlice";
import { ColorModeContext } from "./utils/colorContext";
import io from "socket.io-client";
import { setSocket } from "./store/slice/socketSlice";
import SocketClient from "./SocketClient";
import { getNotifies } from "./store/slice/notifySlice";
import { Suspense, lazy } from "react";
import ScrollToTop from "./utils/scrollToTop";
import Header from "./components/header/Header";
import Sidebar from "./components/sidebar/Sidebar";
import Conversation from "./pages/conversation/Conversation";

const Home = lazy(() => import("./pages/home/Home"));
const Login = lazy(() => import("./pages/login/Login"));
const Profile = lazy(() => import("./pages/profile/Profile"));
const DetailPost = lazy(() => import("./pages/detail-post/DetailPost"));
const Notification = lazy(() => import("./pages/notification/Notification"));
const SuggestionUser = lazy(() => import("./pages/suggestion-user/SuggestionUser"));
const Messages = lazy(() => import("./pages/messages/Messages"));

function App() {
  const dispatch = useDispatch();

  const token = useSelector((state) => state.user.token);
  const userInfo = useSelector((state) => state.user.userInfo);
  const showAlert = useSelector((state) => state.alert.showAlert);
  const messageSuccess = useSelector((state) => state.alert.messageSuccess);
  const messageError = useSelector((state) => state.alert.messageError);
  
  const [mode, setMode] = useState("light");

  const storedMode = localStorage.getItem("mode");
  if (storedMode == null) {
    localStorage.setItem("mode", "light");
  }

  const colorMode = useMemo(
    () => ({
      toggleColorMode: () => {
        if (storedMode === "light" || storedMode === null) {
          localStorage.removeItem("mode");
          localStorage.setItem("mode", "dark");
          setMode("dark");
        } else {
          localStorage.removeItem("mode");
          localStorage.setItem("mode", "light");
          setMode("light");
        }
      },
    }),
    [storedMode]
  );

  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          mode,
        },
      }),
    [mode]
  );

  const handleClose = () => {
    dispatch(setMessageSuccess(""));
    dispatch(setMessageError(""));
  };

  useEffect(() => {
    setMode(storedMode);
  }, [storedMode]);

  useEffect(() => {
    dispatch(refreshToken());
    const socket = io(process.env.REACT_APP_BACK_END_API);
    dispatch(setSocket(socket));

    return () => socket.close();
  }, [dispatch]);

  useEffect(() => {
    if (token) {
      dispatch(getPosts(userInfo._id));
      dispatch(getNotifies(userInfo._id));
      dispatch(getUserSuggestion(userInfo._id));
    }
  }, [dispatch, token, userInfo._id]);

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Router>
          <ScrollToTop />
          <Paper elevation={0}>
            <Backdrop sx={{ color: "#fff", zIndex: 3000 }} open={showAlert}>
              <CircularProgress color="inherit" />
            </Backdrop>
            <Snackbar open={!!messageSuccess} autoHideDuration={3000} onClose={handleClose}>
              <Alert onClose={handleClose} severity="success" sx={{ width: "100%" }}>
                {messageSuccess}
              </Alert>
            </Snackbar>
            <Snackbar open={!!messageError} autoHideDuration={3000} onClose={handleClose}>
              <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
                {messageError}
              </Alert>
            </Snackbar>
            {token && <SocketClient />}
            <Suspense
              fallback={
                <Backdrop sx={{ color: "#fff", zIndex: 3000 }} open={true}>
                  <CircularProgress color="inherit" />
                </Backdrop>
              }>
              <Routes>
                <Route path="/login" element={<Login />} />
                <Route path="/post/:id" element={<DetailPost />} />
                <Route path="/" element={<Layout />}>
                  <Route path="/" element={token ? <Home /> : <Navigate to="/login" />} />
                  <Route path="/profile/:id" element={<Profile />} />
                  <Route path="/notification" element={token ? <Notification /> : <Navigate to="/login" />} />
                  <Route path="/suggestion" element={token ? <SuggestionUser /> : <Navigate to="/login" />} />
                  <Route path="/message" element={token ? <Messages /> : <Navigate to="/login" />} />
                  <Route path="/message/:id" element={token ? <Conversation /> : <Navigate to="/login" />} />
                </Route>
              </Routes>
            </Suspense>
          </Paper>
        </Router>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}

function Layout() {
  const token = useSelector((state) => state.user.token);
  const minWidth1175 = useMediaQuery("(min-width:1175px)");
  const minWidth750 = useMediaQuery("(min-width:750px)");

  return (
    <>
      <Header />
      <Stack direction="row">
        {token && <Sidebar />}
        <Suspense
          fallback={
            <Stack sx={{ width: minWidth1175 ? "80vw" : minWidth750 ? "90vw" : "100vw" }}>
              <LinearProgress />
            </Stack>
          }>
          <Outlet />
        </Suspense>
      </Stack>
    </>
  );
}

export default App;
