import { createSlice } from "@reduxjs/toolkit";

const suggestionSlice = createSlice({
  name: "suggestion",
  initialState: { userSuggestion: [] },
  reducers: {
    getUserSuggestion(state, action) {},

    setUserSuggestion(state, action) {
      return { ...state, userSuggestion: action.payload };
    },

    updateUserSuggestion(state, action) {
      return {
        ...state,
        userSuggestion: state.userSuggestion.map((suggestion) =>
          suggestion === action.payload._id ? action.payload : suggestion
        ),
      };
    },
  },
});

export const { getUserSuggestion, setUserSuggestion, updateUserSuggestion } = suggestionSlice.actions;

export default suggestionSlice.reducer;
