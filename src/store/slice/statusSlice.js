import { createSlice } from "@reduxjs/toolkit";

const statusSlice = createSlice({
  name: "status",
  initialState: { postEdit: {}, showStatus: false },
  reducers: {
    setPostEdit(state, action) {
      return { ...state, postEdit: action.payload };
    },

    setShowStatus(state, action) {
      const bool = action.payload;
      return { ...state, showStatus: bool };
    },
  },
});

export const { setPostEdit, setShowStatus } = statusSlice.actions;

export default statusSlice.reducer;
