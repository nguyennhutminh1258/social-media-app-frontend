import { createSlice } from "@reduxjs/toolkit";

const alertSlice = createSlice({
  name: "alert",
  initialState: {
    showAlert: false,
    messageSuccess: "",
    messageError: "",
  },
  reducers: {
    setShowAlert(state, action) {
      const bool = action.payload;
      return { ...state, showAlert: bool };
    },
    setMessageSuccess(state, action) {
      const message = action.payload;
      return { ...state, messageSuccess: message };
    },
    setMessageError(state, action) {
      const message = action.payload;
      return { ...state, messageError: message };
    },
  },
});

export const { setShowAlert, setMessageSuccess, setMessageError } = alertSlice.actions;

export default alertSlice.reducer;
