import { createSlice } from "@reduxjs/toolkit";

const profileSlice = createSlice({
  name: "profile",
  initialState: {
    userProfile: {},
    posts: [],
    skip: 0,
    limit: 5,
    hasMore: true,
    loadingPost: false,
  },
  reducers: {
    getUserProfile(state, action) {},

    getUserPost(state, action) {},

    deleteUserPost(state, action) {
      return { ...state, posts: state.posts.filter((post) => post._id !== action.payload._id) };
    },

    setUserProfile(state, action) {
      const userData = action.payload;
      return { ...state, userProfile: { ...userData } };
    },

    setUserPost(state, action) {
      return { ...state, posts: [...state.posts, action.payload] };
    },

    setUserPostCreated(state, action) {
      return { ...state, posts: [action.payload, ...state.posts] };
    },

    setEmptyPost(state, action) {
      return {
        ...state,
        posts: [],
        skip: 0,
        limit: 5,
        hasMore: true,
        loadingPost: false,
      };
    },

    setUpdatedUserPosts(state, action) {
      return {
        ...state,
        posts: state.posts.map((post) => (post._id === action.payload._id ? action.payload : post)),
      };
    },

    updateProfilePost(state, action) {
      return {
        ...state,
        posts: state.posts.map((post) =>
          post.user._id === action.payload._id ? { ...post, user: action.payload } : post
        ),
      };
    },

    follow(state, action) {},

    unfollow(state, action) {},

    setSkipProfile(state, action) {
      return { ...state, skip: state.posts.length };
    },

    setLoadingPost(state, action) {
      const bool = action.payload;
      return { ...state, loadingPost: bool };
    },

    setHasMore(state, action) {
      const bool = action.payload;
      return { ...state, hasMore: bool };
    },
  },
});

export const {
  getUserProfile,
  setUserProfile,
  getUserPost,
  deleteUserPost,
  setUserPost,
  setUserPostCreated,
  setUpdatedUserPosts,
  follow,
  unfollow,
  setSkipProfile,
  setLoadingPost,
  setHasMore,
  setEmptyPost,
  updateProfilePost,
} = profileSlice.actions;

export default profileSlice.reducer;
