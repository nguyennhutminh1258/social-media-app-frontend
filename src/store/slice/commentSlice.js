import { createSlice } from "@reduxjs/toolkit";

const commentSlice = createSlice({
  name: "comment",
  initialState: {},
  reducers: {
    createComment(state, action) {},

    updateComment(state, action) {},

    deleteComment(state, action) {},

    likeComment(state, action) {},

    unLikeComment(state, action) {},
  },
});

export const { createComment, updateComment, deleteComment, likeComment, unLikeComment } =
  commentSlice.actions;

export default commentSlice.reducer;
