import { createSlice } from "@reduxjs/toolkit";

const searchSlice = createSlice({
  name: "search",
  initialState: [],
  reducers: {
    searchUser(state, action) {},

    setSearchList(state, action) {
      const listUser = action.payload;
      return [...listUser];
    },
  },
});

export const { searchUser, setSearchList } = searchSlice.actions;

export default searchSlice.reducer;
