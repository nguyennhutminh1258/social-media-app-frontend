import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
  name: "user",
  initialState: {
    userInfo: {},
    token: "",
  },
  reducers: {
    login(state, action) {},

    logout(state, action) {},

    registerUser(state, action) {},

    refreshToken(state, action) {},

    updateUser(state, action) {},

    changePassword(state, action) {},

    getUserInfo(state, action) {},

    setUserInfo(state, action) {
      const userData = action.payload;
      return { ...state, userInfo: userData };
    },

    setToken(state, action) {
      const token = action.payload;
      return { ...state, token: token };
    },
  },
});

export const {
  login,
  logout,
  registerUser,
  refreshToken,
  updateUser,
  changePassword,
  getUserInfo,
  setUserInfo,
  setToken,
} = userSlice.actions;

export default userSlice.reducer;
