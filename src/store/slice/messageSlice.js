import { createSlice } from "@reduxjs/toolkit";

const messageSlice = createSlice({
  name: "message",
  initialState: {
    searchList: [],
    userList: [],
    resultUser: 0,
    data: [],
    resultData: 0,
    firstLoad: true,
    loadingConversation: true,
  },
  reducers: {
    searchUserMess(state, action) {},

    setLoadingConversation(state, action) {
      const bool = action.payload;
      return { ...state, loadingConversation: bool };
    },

    setSearchListMess(state, action) {
      const listUser = action.payload;
      return { ...state, searchList: [...listUser] };
    },

    setUserListMess(state, action) {
      const user = action.payload;
      return { ...state, userList: [user, ...state.userList] };
    },

    addMessage(state, action) {
      return {
        ...state,
        data: [...state.data, action.payload],
        userList: state.userList.map((user) =>
          user._id === action.payload.recipient ||
          user._id === action.payload.sender
            ? {
                ...user,
                text: action.payload.text,
                media: action.payload.media,
              }
            : user
        ),
      };
    },

    setData(state, action) {
      return {
        ...state,
        data: [...state.data, action.payload],
      };
    },

    setUserList(state, action) {
      return {
        ...state,
        userList: state.userList.map((user) =>
          user._id === action.payload.recipient ||
          user._id === action.payload.sender
            ? {
                ...user,
                text: action.payload.text,
                media: action.payload.media,
              }
            : user
        ),
      };
    },

    getConversations(state, action) {},

    setConversations(state, action) {
      return {
        ...state,
        userList: [...state.userList, ...action.payload],
      };
    },

    setUpdatedConversation(state, action) {
      return {
        ...state,
        userList: state.userList.map((item) =>
          item._id === action.payload._id ? action.payload : item
        ),
      };
    },

    getMessages(state, action) {},

    setMessages(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },

    setFirstLoad(state, action) {
      return { ...state, firstLoad: action.payload };
    },

    deleteMessage(state, action) {},
  },
});

export const {
  searchUserMess,
  setSearchListMess,
  setUserListMess,
  addMessage,
  getConversations,
  setConversations,
  getMessages,
  setUserList,
  setData,
  setMessages,
  setFirstLoad,
  setLoadingConversation,
  deleteMessage,
  setUpdatedConversation,
} = messageSlice.actions;

export default messageSlice.reducer;
