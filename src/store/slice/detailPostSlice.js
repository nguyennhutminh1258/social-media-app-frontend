import { createSlice } from "@reduxjs/toolkit";

const detailPostSlice = createSlice({
  name: "detailPost",
  initialState: {},
  reducers: {
    getDetailPost(state, action) {},

    setDetailPost(state, action) {
      const userData = action.payload;
      return { ...userData };
    },

    setUpdatedDetailPost(state, action) {
      const userData = action.payload;
      return { ...userData };
    },

    setEmptyDetailPost(state, action) {
      return {};
    },
  },
});

export const { getDetailPost, setDetailPost, setUpdatedDetailPost, setEmptyDetailPost } =
  detailPostSlice.actions;

export default detailPostSlice.reducer;
