import { createSlice } from "@reduxjs/toolkit";

const notifySlice = createSlice({
  name: "notify",
  initialState: [],
  reducers: {
    createNotify(state, action) {},

    removeNotify(state, action) {},

    getNotifies(state, action) {},

    setNotify(state, action) {
      return [...action.payload];
    },

    readNotify(state, action) {},

    deleteAllNotify(state, action) {},
  },
});

export const { createNotify, removeNotify, getNotifies, setNotify, readNotify, deleteAllNotify } =
  notifySlice.actions;

export default notifySlice.reducer;
