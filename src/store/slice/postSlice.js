import { createSlice } from "@reduxjs/toolkit";

const postSlice = createSlice({
  name: "post",
  initialState: {
    posts: [],
    skip: 0,
    limit: 5,
    hasMore: true,
    loadingPost: false,
    onEdit: false,
  },
  reducers: {
    createPost(state, action) {},

    getPosts(state, action) {},

    updatePost(state, action) {},

    deletePost(state, action) {
      return { ...state, posts: state.posts.filter((post) => post._id !== action.payload._id) };
    },

    likePost(state, action) {},

    unLikePost(state, action) {},

    setPosts(state, action) {
      return { ...state, posts: [...state.posts, action.payload] };
    },

    setPostsCreated(state, action) {
      return { ...state, posts: [action.payload, ...state.posts] };
    },

    setUpdatedPosts(state, action) {
      return {
        ...state,
        posts: state.posts.map((post) => (post._id === action.payload._id ? action.payload : post)),
      };
    },

    updateUserPost(state, action) {
      return {
        ...state,
        posts: state.posts.map((post) =>
          post.user._id === action.payload._id ? { ...post, user: action.payload } : post
        ),
      };
    },

    setSkip(state, action) {
      return { ...state, skip: state.posts.length };
    },

    setOnEdit(state, action) {
      const bool = action.payload;
      return { ...state, onEdit: bool };
    },

    setLoadingPost(state, action) {
      const bool = action.payload;
      return { ...state, loadingPost: bool };
    },

    setHasMore(state, action) {
      const bool = action.payload;
      return { ...state, hasMore: bool };
    },
  },
});

export const {
  createPost,
  getPosts,
  updatePost,
  deletePost,
  likePost,
  unLikePost,
  setPosts,
  setSkip,
  setOnEdit,
  setUpdatedPosts,
  setLoadingPost,
  setHasMore,
  updateUserPost,
  setPostsCreated,
} = postSlice.actions;

export default postSlice.reducer;
