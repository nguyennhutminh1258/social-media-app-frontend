import { call, put } from "redux-saga/effects";
import { setUpdatedDetailPost } from "../../../slice/detailPostSlice";
import { setMessageError, setMessageSuccess } from "../../../slice/alertSlice";
import { setUpdatedPosts } from "../../../slice/postSlice";
import { setUpdatedUserPosts } from "../../../slice/profileSlice";
import { requestUpdateComment } from "../../requests/comment/requestUpdateComment";

export function* handleUpdateComment(action) {
  try {
    const { post, comment, content, user } = action.payload;

    const newComment = post.comments.map((item) =>
      item._id === comment._id ? { ...item, content: content } : item
    );

    const newPost = {
      ...post,
      comments: newComment,
    };
    yield put(setUpdatedPosts(newPost));
    yield put(setUpdatedUserPosts(newPost));
    yield put(setUpdatedDetailPost(newPost));

    const data = {
      content: content,
      idComment: comment._id,
      idUser: user._id,
    };
    const res = yield call(requestUpdateComment, {
      url: "comment",
      data,
    });

    yield put(setMessageSuccess(res.data.msg));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
