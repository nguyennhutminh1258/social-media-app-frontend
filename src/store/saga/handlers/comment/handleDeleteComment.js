import { call, put } from "redux-saga/effects";
import { setUpdatedDetailPost } from "../../../slice/detailPostSlice";
import { setMessageError } from "../../../slice/alertSlice";
import { setUpdatedPosts } from "../../../slice/postSlice";
import { setUpdatedUserPosts } from "../../../slice/profileSlice";
import { requestDeleteComment } from "../../requests/comment/requestDeleteComment";
import { removeNotify } from "../../../slice/notifySlice";

export function* handleDeleteComment(action) {
  try {
    const { post, comment, userInfo, socket } = action.payload;

    const deleteArr = [
      ...post.comments.filter((item) => item.reply === comment._id),
      comment,
    ];

    for (let item of deleteArr) {
      const notify = {
        id: item._id,
        text: comment.reply
          ? "nhắc đến bạn trong một bình luận"
          : "vừa bình luận bài viết của bạn",
        recipients: comment.reply ? [comment.tag._id] : [post.user._id],
        url: `/post/${post._id}`,
        user: userInfo._id,
      };
      const data = {
        idComment: item._id,
        idUserPost: post.user._id,
        idUser: userInfo._id,
      };

      yield call(requestDeleteComment, {
        url: "comment",
        data,
      });
      yield put(removeNotify(notify));
    }

    const newPost = {
      ...post,
      comments: post.comments.filter(
        (item) => !deleteArr.find((deleteItem) => deleteItem._id === item._id)
      ),
    };

    yield put(setUpdatedPosts(newPost));
    yield put(setUpdatedUserPosts(newPost));
    yield put(setUpdatedDetailPost(newPost));

    socket.emit("deleteComment", newPost);
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
