import { call, put } from "redux-saga/effects";
import { setUpdatedDetailPost } from "../../../slice/detailPostSlice";
import { setMessageError } from "../../../slice/alertSlice";
import { setUpdatedPosts } from "../../../slice/postSlice";
import { setUpdatedUserPosts } from "../../../slice/profileSlice";
import { requestLikeComment } from "../../requests/comment/requestLikeComment";

export function* handleLikeComment(action) {
  const payload = {
    url: "comment",
    data: action.payload,
  };

  try {
    const newComment = {
      ...action.payload.comment,
      likes: [...action.payload.comment.likes, action.payload.userInfo],
    };
    
    const newPost = {
      ...action.payload.post,
      comments: action.payload.post.comments.map((comment) =>
        comment._id === newComment._id ? newComment : comment
      ),
    };

    yield put(setUpdatedPosts(newPost));
    yield put(setUpdatedUserPosts(newPost));
    yield put(setUpdatedDetailPost(newPost));

    yield call(requestLikeComment, payload);
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
