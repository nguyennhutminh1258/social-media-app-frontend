import { call, put, select } from "redux-saga/effects";
import { setUpdatedDetailPost } from "../../../slice/detailPostSlice";
import { setMessageError } from "../../../slice/alertSlice";
import { setUpdatedPosts } from "../../../slice/postSlice";
import { setUpdatedUserPosts } from "../../../slice/profileSlice";
import { requestCreateComment } from "../../requests/comment/requestCreateComment";
import { createNotify } from "../../../slice/notifySlice";

export function* handleCreateComment(action) {
  try {
    const { post, newComment, socket } = action.payload;
    const userInfo = yield select((state) => state.user.userInfo);
    const newPost = { ...post, comments: [newComment, ...post.comments] };

    const data = {
      content: newComment.content,
      idPost: post._id,
      idUserPost: post.user._id,
      idUser: newComment.user._id,
      tag: newComment.tag,
      reply: newComment.reply,
    };
    yield put(setUpdatedPosts(newPost));
    yield put(setUpdatedUserPosts(newPost));
    yield put(setUpdatedDetailPost(newPost));

    const res = yield call(requestCreateComment, {
      url: "comment",
      data,
    });

    const newData = { ...res.data.newComment, user: newComment.user };
    const newPostUpdated = { ...post, comments: [newData, ...post.comments] };

    const notify = {
      id: res.data.newComment._id,
      text: newComment.reply
        ? "nhắc đến bạn trong một bình luận"
        : "vừa bình luận bài viết của bạn",
      recipients: newComment.reply
        ? [newComment.tag._id]
        : [action.payload.post.user._id],
      url: `/post/${action.payload.post._id}`,
      content: post.content,
      image: post.images[0].url,
      user: userInfo._id,
    };
    if (userInfo._id !== post.user._id) {
      yield put(createNotify(notify));
    }

    yield put(setUpdatedPosts(newPostUpdated));
    yield put(setUpdatedUserPosts(newPostUpdated));
    yield put(setUpdatedDetailPost(newPostUpdated));

    socket.emit("createComment", newPostUpdated);
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
