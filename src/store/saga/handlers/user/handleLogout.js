import { call, put } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { requestLogout } from "../../requests/user/requestLogout";

export function* handleLogout(action) {
  const payload = {
    url: "logout",
  };
  
  try {
    localStorage.removeItem("first-login");
    localStorage.setItem("mode", "light");
    yield call(requestLogout, payload);
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
