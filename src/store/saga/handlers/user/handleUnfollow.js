import { call, put, select } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { removeNotify } from "../../../slice/notifySlice";
import { getUserProfile } from "../../../slice/profileSlice";
import { getUserSuggestion } from "../../../slice/suggestionSlice";
import { getUserInfo } from "../../../slice/userSlice";
import { requestUnfollow } from "../../requests/user/requestUnfollow";

export function* handleUnfollow(action) {
  const payload = {
    url: "unfollow",
    data: action.payload,
  };

  try {
    const userProfile = yield select((state) => state.profile.userProfile);
    const userInfo = yield select((state) => state.user.userInfo);
    const socket = yield select((state) => state.socket);

    const res = yield call(requestUnfollow, payload);

    yield socket.emit("unFollow", res.data.newUser);
    const notify = {
      id: res.data.newUser._id,
      text: "đã bắt đầu theo dõi bạn",
      recipients: [res.data.newUser._id],
      url: `/profile/${userInfo._id}`,
      user: userInfo._id,
    };
    yield put(removeNotify(notify));
    
    // Check object is not null
    if (Object.keys(userProfile).length > 0) {
      yield put(getUserProfile(userProfile._id));
    }
    yield put(getUserInfo(userInfo._id));
    yield put(getUserSuggestion(userInfo._id));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
