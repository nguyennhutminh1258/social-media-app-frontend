import { call, put } from "redux-saga/effects";
import { setMessageError, setShowAlert } from "../../../slice/alertSlice";
import { setToken, setUserInfo } from "../../../slice/userSlice";
import { requestRefreshToken } from "../../requests/user/requestRefeshToken";

export function* handleRefreshToken(action) {
  const payload = {
    url: "refresh_token",
  };

  const firstLogin = localStorage.getItem("first-login");

  if (firstLogin) {
    yield put(setShowAlert(true));
    try {
      const res = yield call(requestRefreshToken, payload);
      yield put(setUserInfo(res.data.user));
      yield put(setToken(res.data.accessToken));
    } catch (error) {
      yield put(setMessageError(error.response.data.msg));
    }
    yield put(setShowAlert(false));
  }
}
