import { call, put } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { setUserSuggestion } from "../../../slice/suggestionSlice";
import { requestGetSuggestion } from "../../requests/user/requestGetSuggestion";

export function* handleGetSuggestion(action) {
  const payload = {
    url: "suggestionUser",
    id: action.payload,
  };
  
  try {
    const res = yield call(requestGetSuggestion, payload);
    yield put(setUserSuggestion(res.data.userSuggestion));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
