import { call, put } from "redux-saga/effects";
import { setMessageError, setMessageSuccess, setShowAlert } from "../../../slice/alertSlice";
import { requestChangePassword } from "../../requests/user/requestChangePassword";

export function* handleChangePassword(action) {
  const payload = {
    url: "user_password",
    data: action.payload,
  };
  
  yield put(setShowAlert(true));
  try {
    // Change password request
    const res = yield call(requestChangePassword, payload);
    // Hiện thông báo thay đổi mật khẩu thành công
    yield put(setMessageSuccess(res.data.msg));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
  yield put(setShowAlert(false));
}
