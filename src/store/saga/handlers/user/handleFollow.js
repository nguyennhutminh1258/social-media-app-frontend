import { call, put, select } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { createNotify } from "../../../slice/notifySlice";
import { getUserProfile } from "../../../slice/profileSlice";
import { getUserSuggestion } from "../../../slice/suggestionSlice";
import { getUserInfo } from "../../../slice/userSlice";
import { requestFollow } from "../../requests/user/requestFollow";

export function* handleFollow(action) {
  const payload = {
    url: "follow",
    data: action.payload,
  };

  try {
    const userProfile = yield select((state) => state.profile.userProfile);
    const userInfo = yield select((state) => state.user.userInfo);
    const socket = yield select((state) => state.socket);

    const res = yield call(requestFollow, payload);
    
    if (res.data.newUser._id !== userInfo._id) {
      yield socket.emit("follow", res.data.newUser);
      const notify = {
        id: res.data.newUser._id,
        text: "đã bắt đầu theo dõi bạn",
        recipients: [res.data.newUser._id],
        url: `/profile/${userInfo._id}`,
        user: userInfo._id,
      };
      yield put(createNotify(notify));
    }

    if (Object.keys(userProfile).length > 0) {
      yield put(getUserProfile(userProfile._id));
    }
    yield put(getUserInfo(userInfo._id));
    yield put(getUserSuggestion(userInfo._id));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
