import { call, put, select } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { setUserPost, setLoadingPost, setSkipProfile, setHasMore } from "../../../slice/profileSlice";
import { requestGetUserPost } from "../../requests/user/requestGetUserPost";

export function* handleGetUserPost(action) {
  const payload = {
    url: "user_post",
    id: action.payload,
  };
  
  const skip = yield select((state) => state.profile.skip);
  const limit = yield select((state) => state.profile.limit);
  
  if (skip !== 0) {
    yield put(setLoadingPost(true));
  }
  try {
    const res = yield call(requestGetUserPost, { ...payload, skip, limit });
    yield put(setHasMore(res.data.posts.length > 0));
    for (const index in res.data.posts) {
      yield put(setUserPost(res.data.posts[index]));
    }
    yield put(setSkipProfile());
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
  yield put(setLoadingPost(false));
}
