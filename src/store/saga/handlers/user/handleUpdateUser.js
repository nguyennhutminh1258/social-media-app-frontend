import { call, put, select } from "redux-saga/effects";
import { imageUpload } from "../../../../utils/imageUpload";
import { setMessageError, setMessageSuccess, setShowAlert } from "../../../slice/alertSlice";
import { updateUserPost } from "../../../slice/postSlice";
import { getUserProfile, updateProfilePost } from "../../../slice/profileSlice";
import { getUserInfo } from "../../../slice/userSlice";
import { requestUpdateUser } from "../../requests/user/requestUpdateUser";

export function* handleUpdateUser(action) {
  const payload = {
    url: "user",
    data: action.payload,
  };

  yield put(setShowAlert(true));
  try {
    const userInfo = yield select((state) => state.user.userInfo);
    const userProfilePosts = yield select((state) => state.profile.posts);
    const posts = yield select((state) => state.post.posts);

    let media;
    if (action.payload.avatar) media = yield imageUpload([action.payload.avatar]);
    
    const res = yield call(requestUpdateUser, {
      ...payload,
      data: {
        ...payload.data,
        avatar: payload.data.avatar ? media[0].url : userInfo.avatar,
      },
    });

    yield put(getUserProfile(userInfo._id));
    yield put(getUserInfo(userInfo._id));

    if (userProfilePosts.length > 0) {
      yield put(updateProfilePost(res.data.newUser));
    }
    if (posts.length > 0 && posts.find((post) => post.user._id === res.data.newUser._id)) {
      yield put(updateUserPost(res.data.newUser));
    }

    yield put(setMessageSuccess(res.data.msg));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
  yield put(setShowAlert(false));
}
