import { call, put } from "redux-saga/effects";
import { setMessageError, setMessageSuccess, setShowAlert } from "../../../slice/alertSlice";
import { setToken, setUserInfo } from "../../../slice/userSlice";
import { requestRegisterUser } from "../../requests/user/requestRegisterUser";

export function* handleRegisterUser(action) {
  const payload = {
    url: "register",
    data: action.payload,
  };
  
  yield put(setShowAlert(true));
  try {
    const res = yield call(requestRegisterUser, payload);
    yield put(setUserInfo(res.data.user));
    yield put(setToken(res.data.accessToken));
    yield put(setMessageSuccess(res.data.msg));
    localStorage.setItem("first-login", true);
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
  yield put(setShowAlert(false));
}
