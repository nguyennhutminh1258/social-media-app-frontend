import { call, put } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { setUserInfo } from "../../../slice/userSlice";
import { requestGetUser } from "../../requests/user/requestGetUser";

export function* handleGetUser(action) {
  const payload = {
    url: "user",
    id: action.payload,
  };
  
  try {
    const res = yield call(requestGetUser, payload);
    yield put(setUserInfo(res.data.user));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
