import { call, put } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { setUserProfile } from "../../../slice/profileSlice";
import { requestGetUserProfile } from "../../requests/user/requestGetUserProfile";

export function* handleGetUserProfile(action) {
  const payload = {
    url: "user",
    id: action.payload,
  };

  try {
    // Lấy thông tin user ở trang profile
    const res = yield call(requestGetUserProfile, payload);
    yield put(setUserProfile(res.data.user));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
