import { call, put } from "redux-saga/effects";
import { setMessageError, setMessageSuccess, setShowAlert } from "../../../slice/alertSlice";
import { setToken, setUserInfo } from "../../../slice/userSlice";
import { requestLogin } from "../../requests/user/requestLogin";

export function* handleLogin(action) {
  const payload = {
    url: "login",
    data: action.payload,
  };

  yield put(setShowAlert(true));
  try {
    // Login request
    const res = yield call(requestLogin, payload);
    // Thiết lập thông tin user vào state
    yield put(setUserInfo(res.data.user));
    yield put(setToken(res.data.accessToken));
    // Hiện thông báo đăng nhập thành công
    yield put(setMessageSuccess(res.data.msg));
    localStorage.setItem("first-login", true);
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
  yield put(setShowAlert(false));
}
