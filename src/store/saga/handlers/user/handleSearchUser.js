import { call, put } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { setSearchList } from "../../../slice/searchSlice";
import { requestSearchUser } from "../../requests/user/requestSearchUser";

export function* handleSearchUser(action) {
  const payload = {
    url: "search?userName=",
    search: action.payload,
  };
  
  try {
    if (action.payload === "") {
      yield put(setSearchList([]));
    } else {
      const res = yield call(requestSearchUser, payload);
      yield put(setSearchList(res.data.users));
    }
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
