import { call, put, select } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import {
  setMessages,
  setUpdatedConversation,
} from "../../../slice/messageSlice";
import { requestDeleteMessage } from "../../requests/message/requestDeleteMessage";

export function* handleDeleteMessage(action) {
  try {
    const socket = yield select((state) => state.socket);
    const userInfo = yield select((state) => state.user.userInfo);
    const messages = yield select((state) => state.message.data);
    const userList = yield select((state) => state.message.userList);

    const data = {
      idMessage: action.payload._id,
      idSender: userInfo._id,
      idRecipient: action.payload.recipient,
    };
    const newMessages = messages.filter(
      (message) => message._id !== action.payload._id
    );
    const lastMess = newMessages[newMessages.length - 1];
    const newConversation = {
      ...userList.find((item) => item._id === action.payload.recipient),
    };

    yield put(setMessages(newMessages));
    yield put(
      setUpdatedConversation({ ...newConversation, text: lastMess.text })
    );

    socket.emit("deleteMessage", action.payload);

    yield call(requestDeleteMessage, { url: "message", data });
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
