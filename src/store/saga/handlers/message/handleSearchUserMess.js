import { call, put } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { setSearchListMess } from "../../../slice/messageSlice";
import { requestSearchUser } from "../../requests/user/requestSearchUser";

export function* handleSearchUserMess(action) {
  const payload = {
    url: "search?userName=",
    search: action.payload,
  };

  try {
    if (action.payload === "") {
      yield put(setSearchListMess([]));
    } else {
      const res = yield call(requestSearchUser, payload);
      yield put(setSearchListMess(res.data.users));
    }
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
