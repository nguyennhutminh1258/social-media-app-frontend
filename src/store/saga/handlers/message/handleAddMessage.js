import { call, put, select } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { requestAddMessage } from "../../requests/message/requestAddMessage";

export function* handleAddMessage(action) {
  try {
    const socket = yield select((state) => state.socket);
    const userInfo = yield select((state) => state.user.userInfo);
    const data = {
      ...action.payload,
      idUser: userInfo._id,
    };

    socket.emit("addMessage", action.payload);

    yield call(requestAddMessage, { url: "message", data });
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
