import { call, put, select } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import {
  setLoadingConversation,
  setMessages,
} from "../../../slice/messageSlice";
import { requestGetMessages } from "../../requests/message/requestGetMessages";

export function* handleGetMessages(action) {
  try {
    const userInfo = yield select((state) => state.user.userInfo);
    const data = {
      sender: userInfo._id,
      recipient: action.payload,
    };

    yield put(setLoadingConversation(true));

    try {
      const res = yield call(requestGetMessages, {
        url: "messages",
        data,
      });

      yield put(setMessages(res.data.messages));
    } catch (error) {}
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
  yield put(setLoadingConversation(false));
}
