import { call, put, select } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { setConversations } from "../../../slice/messageSlice";
import { requestGetConversations } from "../../requests/message/requestGetConversations";

export function* handleGetConversations(action) {
  const payload = {
    url: "conversations",
    id: action.payload,
  };

  try {
    const userInfo = yield select((state) => state.user.userInfo);
    let newConversation = [];

    const res = yield call(requestGetConversations, payload);
    yield res.data.conversations.forEach((item) => {
      item.recipients.forEach((conversation) => {
        if (conversation._id !== userInfo._id) {
          newConversation.push({ ...conversation, text: item.text, media: item.media });
        }
      });
    });

    yield put(setConversations(newConversation));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
