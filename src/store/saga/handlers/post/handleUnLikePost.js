import { call, put } from "redux-saga/effects";
import { setUpdatedDetailPost } from "../../../slice/detailPostSlice";
import { setMessageError } from "../../../slice/alertSlice";
import { setUpdatedPosts } from "../../../slice/postSlice";
import { setUpdatedUserPosts } from "../../../slice/profileSlice";
import { requestUnLikePost } from "../../requests/post/requestUnLikePost";
import { removeNotify } from "../../../slice/notifySlice";

export function* handleUnLikePost(action) {
  const payload = {
    url: "post",
    data: action.payload,
  };
  
  try {
    const newPost = {
      ...action.payload.post,
      likes: action.payload.post.likes.filter((like) => like._id !== action.payload.userInfo._id),
    };
    action.payload.socket.emit("unLikePost", newPost);

    const notify = {
      id: action.payload.userInfo._id,
      text: "vừa thich bài viết của bạn",
      recipients: [action.payload.post.user._id],
      url: `/post/${action.payload.post._id}`,
      user: action.payload.userInfo._id,
    };
    yield put(removeNotify(notify));

    yield put(setUpdatedPosts(newPost));
    yield put(setUpdatedUserPosts(newPost));
    yield put(setUpdatedDetailPost(newPost));

    yield call(requestUnLikePost, payload);
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
