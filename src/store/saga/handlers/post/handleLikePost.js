import { call, put } from "redux-saga/effects";
import { setUpdatedDetailPost } from "../../../slice/detailPostSlice";
import { setMessageError } from "../../../slice/alertSlice";
import { setUpdatedPosts } from "../../../slice/postSlice";
import { setUpdatedUserPosts } from "../../../slice/profileSlice";
import { requestLikePost } from "../../requests/post/requestLikePost";
import { createNotify } from "../../../slice/notifySlice";

export function* handleLikePost(action) {
  const payload = {
    url: "post",
    data: action.payload,
  };

  try {
    const newPost = {
      ...action.payload.post,
      likes: [...action.payload.post.likes, action.payload.userInfo],
    };

    action.payload.socket.emit("likePost", newPost);
    const notify = {
      id: action.payload.userInfo._id,
      text: "vừa thich bài viết của bạn",
      recipients: [action.payload.post.user._id],
      url: `/post/${action.payload.post._id}`,
      content: action.payload.post.content,
      image: action.payload.post.images[0].url,
      user: action.payload.userInfo._id,
    };

    if (action.payload.userInfo._id !== action.payload.post.user._id) {
      yield put(createNotify(notify));
    }
    yield put(setUpdatedPosts(newPost));
    yield put(setUpdatedUserPosts(newPost));
    yield put(setUpdatedDetailPost(newPost));
    
    yield call(requestLikePost, payload);
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
