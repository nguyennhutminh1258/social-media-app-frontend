import { call, put, select } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { setHasMore, setLoadingPost, setPosts, setSkip } from "../../../slice/postSlice";
import { requestGetPosts } from "../../requests/post/requestGetPosts";

export function* handleGetPosts(action) {
  const payload = {
    url: "post",
    id: action.payload,
  };

  const skip = yield select((state) => state.post.skip);
  const limit = yield select((state) => state.post.limit);

  if (skip !== 0) {
    yield put(setLoadingPost(true));
  }
  try {
    const res = yield call(requestGetPosts, { ...payload, skip, limit });
    
    yield put(setHasMore(res.data.posts.length > 0));
    for (const index in res.data.posts) {
      yield put(setPosts(res.data.posts[index]));
    }
    yield put(setSkip());
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
  yield put(setLoadingPost(false));
}
