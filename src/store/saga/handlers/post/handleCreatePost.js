import { call, put, select } from "redux-saga/effects";
  import { imageUpload } from "../../../../utils/imageUpload";
import { setMessageError, setMessageSuccess, setShowAlert } from "../../../slice/alertSlice";
import { createNotify } from "../../../slice/notifySlice";
import { setPostsCreated, setSkip } from "../../../slice/postSlice";
import { setUserPostCreated, setSkipProfile } from "../../../slice/profileSlice";
import { setShowStatus } from "../../../slice/statusSlice";
import { requestCreatePost } from "../../requests/post/requestCreatePost";

export function* handleCreatePost(action) {
  const payload = {
    url: "post",
    data: action.payload,
  };

  yield put(setShowAlert(true));
  try {
    let media = [];
    const userInfo = yield select((state) => state.user.userInfo);
    const userProfile = yield select((state) => state.profile.userProfile);
    // upload hình ảnh
    if (action.payload.images.length > 0) media = yield imageUpload(action.payload.images);
    // Request create post
    const res = yield call(requestCreatePost, {
      ...payload,
      data: { ...payload.data, images: media },
    });

    const newPost = { ...res.data.newPost, user: userInfo };
    // Tạo thông báo
    const notify = {
      id: newPost._id,
      text: "vừa đăng một bài viết.",
      recipients: [...newPost.user.followers.map((follower) => follower._id)],
      url: `/post/${newPost._id}`,
      content: newPost.content,
      image: media[0].url,
      user: newPost.user._id,
    };
    yield put(createNotify(notify));

    // Cập nhật state sau khi tao bài viết mới
    yield put(setPostsCreated(newPost));
    yield put(setSkip());
    // Nếu user tạo bài viết ở trang profile
    if (Object.keys(userProfile).length > 0 && userInfo._id === userProfile._id) {
      yield put(setUserPostCreated(newPost));
      yield put(setSkipProfile());
    }
    yield put(setShowStatus(false));
    yield put(setMessageSuccess(res.data.msg));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
  yield put(setShowAlert(false));
}
