import { call, put } from "redux-saga/effects";
import { setMessageError, setMessageSuccess } from "../../../slice/alertSlice";
import { removeNotify } from "../../../slice/notifySlice";
import { requestDeletePost } from "../../requests/post/requestDeletePost";

export function* handleDeletePost(action) {
  try {
    const data = {
      idUserPost: action.payload.user._id,
      idPost: action.payload._id,
    };

    const res = yield call(requestDeletePost, {
      url: "post",
      data,
    });

    const notify = {
      id: res.data.post._doc._id,
      text: "vừa đăng một bài viết.",
      recipients: res.data.post.user.followers,
      url: `/post/${res.data.post._doc._id}`,
      user: res.data.post.user._id,
    };
    yield put(removeNotify(notify));
    
    yield put(setMessageSuccess(res.data.msg));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
