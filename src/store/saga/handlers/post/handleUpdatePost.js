import { call, put, select } from "redux-saga/effects";
import { imageUpload } from "../../../../utils/imageUpload";
import { setUpdatedDetailPost } from "../../../slice/detailPostSlice";
import { setMessageError, setMessageSuccess, setShowAlert } from "../../../slice/alertSlice";
import { setOnEdit, setUpdatedPosts } from "../../../slice/postSlice";
import { setUpdatedUserPosts } from "../../../slice/profileSlice";
import { setShowStatus } from "../../../slice/statusSlice";
import { requestUpdatePost } from "../../requests/post/requestUpdatePost";

export function* handleUpdatePost(action) {
  const payload = {
    url: "post",
    data: action.payload,
  };

  yield put(setShowAlert(true));
  try {
    const postEdit = yield select((state) => state.status.postEdit);
    const imageNewUrl = yield action.payload.images.filter((image) => !image.url);
    const imageOldUrl = yield action.payload.images.filter((image) => image.url);
    let media = [];

    if (
      action.payload.content === postEdit.content &&
      imageNewUrl.length === 0 &&
      imageOldUrl.length === postEdit.images.length
    ) {
      yield put(setShowStatus(false));
      yield put(setOnEdit(false));
    } else {
      if (imageNewUrl.length > 0) media = yield imageUpload(imageNewUrl);

      const res = yield call(requestUpdatePost, {
        ...payload,
        data: { ...payload.data, images: [...imageOldUrl, ...media] },
      });

      yield put(setUpdatedPosts(res.data.newPost));
      yield put(setUpdatedUserPosts(res.data.newPost));
      yield put(setUpdatedDetailPost(res.data.newPost));
      
      yield put(setShowStatus(false));
      yield put(setOnEdit(false));
      yield put(setMessageSuccess(res.data.msg));
    }
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
  yield put(setShowAlert(false));
}
