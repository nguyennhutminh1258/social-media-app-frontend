import { call, put } from "redux-saga/effects";
import { setDetailPost } from "../../../slice/detailPostSlice";
import { setMessageError, setShowAlert } from "../../../slice/alertSlice";
import { requestGetDetailPost } from "../../requests/post/requestGetDetailPost";

export function* handleGetDetailPost(action) {
  const payload = {
    url: "detail_post",
    id: action.payload,
  };
  
  yield put(setShowAlert(true));
  try {
    const res = yield call(requestGetDetailPost, payload);

    yield put(setDetailPost(res.data.post));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
  yield put(setShowAlert(false));
}
