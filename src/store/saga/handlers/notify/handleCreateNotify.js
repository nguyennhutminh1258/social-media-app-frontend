import { call, put, select } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { requestCreateNotify } from "../../requests/notify/requestCreateNotify";

export function* handleCreateNotify(action) {
  const payload = {
    url: "notify",
    data: action.payload,
  };
  
  try {
    const socket = yield select((state) => state.socket);
    const userInfo = yield select((state) => state.user.userInfo);

    const res = yield call(requestCreateNotify, payload);

    socket.emit("createNotify", {
      ...res.data.newNotify,
      user: {
        userName: userInfo.userName,
        avatar: userInfo.avatar,
      },
    });
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
