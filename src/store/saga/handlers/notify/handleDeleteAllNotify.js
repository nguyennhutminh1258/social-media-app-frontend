import { call, put } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { setNotify } from "../../../slice/notifySlice";
import { requestDeleteAllNotify } from "../../requests/notify/requestDeleteAllNotify";
export function* handleDeleteAllNotify(action) {
  const payload = {
    url: "delete_all_notify",
    id: action.payload,
  };
  
  try {
    yield call(requestDeleteAllNotify, payload);
    yield put(setNotify([]));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
