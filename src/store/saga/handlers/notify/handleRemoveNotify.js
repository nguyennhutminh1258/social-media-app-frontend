import { call, put, select } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { requestRemoveNotify } from "../../requests/notify/requestRemoveNotify";

export function* handleRemoveNotify(action) {
  const payload = {
    url: "notify",
    data: action.payload,
  };
  
  try {
    const socket = yield select((state) => state.socket);

    yield call(requestRemoveNotify, payload);

    socket.emit("removeNotify", action.payload);
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
