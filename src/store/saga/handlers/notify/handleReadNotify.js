import { call, put, select } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { setNotify } from "../../../slice/notifySlice";
import { requestReadNotify } from "../../requests/notify/requestReadNotify";

export function* handleReadNotify(action) {
  const payload = {
    url: "is_read",
    id: action.payload,
  };
  
  try {
    const notifies = yield select((state) => state.notify);
    yield call(requestReadNotify, payload);

    const newNotifies = notifies.map((notify) =>
      notify._id === action.payload ? { ...notify, isRead: true } : notify
    );

    yield put(setNotify(newNotifies));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
