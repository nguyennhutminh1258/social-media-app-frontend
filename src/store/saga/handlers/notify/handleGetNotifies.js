import { call, put } from "redux-saga/effects";
import { setMessageError } from "../../../slice/alertSlice";
import { setNotify } from "../../../slice/notifySlice";
import { requestGetNotifies } from "../../requests/notify/requestGetNotifies";

export function* handleGetNotifies(action) {
  const payload = {
    url: "notify",
    id: action.payload,
  };
  
  try {
    const res = yield call(requestGetNotifies, payload);
    yield put(setNotify(res.data.notifies));
  } catch (error) {
    yield put(setMessageError(error.response.data.msg));
  }
}
