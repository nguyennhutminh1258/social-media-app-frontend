import app from "../../../../utils/axiosConfig";

export function requestCreateComment(payload) {
  const { url, data } = payload;
  return app.post(`api/${url}`, data);
}
