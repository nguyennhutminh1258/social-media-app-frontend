import app from "../../../../utils/axiosConfig";

export function requestUpdateComment(payload) {
  const { url, data } = payload;
  
  return app.patch(`api/${url}`, data);
}
