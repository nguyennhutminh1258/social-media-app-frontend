import app from "../../../../utils/axiosConfig";

export function requestLikeComment(payload) {
  const { url, data } = payload;
  
  return app.patch(`api/${url}/${data.comment._id}/like`, {
    idUser: data.userInfo._id,
  });
}
