import app from "../../../../utils/axiosConfig";

export function requestUnLikeComment(payload) {
  const { url, data } = payload;
  
  return app.patch(`api/${url}/${data.comment._id}/unlike`, {
    idUser: data.userInfo._id,
  });
}
