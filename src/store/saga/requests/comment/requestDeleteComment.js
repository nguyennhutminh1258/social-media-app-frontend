import app from "../../../../utils/axiosConfig";

export function requestDeleteComment(payload) {
  const { url, data } = payload;
  
  return app.delete(`api/${url}/${data.idComment}`, {
    data: { idUser: data.idUser, idUserPost: data.idUserPost },
  });
}
