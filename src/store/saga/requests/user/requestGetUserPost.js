import app from "../../../../utils/axiosConfig";

export function requestGetUserPost(payload) {
  const { url, id, skip, limit } = payload;
  
  return app.get(`api/${url}/${id}?skip=${skip}&limit=${limit}`);
}
