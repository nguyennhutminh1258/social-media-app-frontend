import app from "../../../../utils/axiosConfig";

export function requestLogin(payload) {
  const { url, data } = payload;
  
  return app.post(`api/${url}`, data);
}
