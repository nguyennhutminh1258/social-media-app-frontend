import app from "../../../../utils/axiosConfig";

export function requestLogout(payload) {
  const { url } = payload;
  
  return app.post(`api/${url}`);
}
