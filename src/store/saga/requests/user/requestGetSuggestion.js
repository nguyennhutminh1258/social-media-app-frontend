import app from "../../../../utils/axiosConfig";

export function requestGetSuggestion(payload) {
  const { url, id } = payload;
  
  return app.get(`api/${url}/${id}`);
}
