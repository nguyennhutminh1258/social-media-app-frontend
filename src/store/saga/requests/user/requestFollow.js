import app from "../../../../utils/axiosConfig";

export function requestFollow(payload) {
  const { url, data } = payload;
  
  return app.patch(`api/${url}`, data);
}
