import app from "../../../../utils/axiosConfig";

export function requestChangePassword(payload) {
  const { url, data } = payload;
  
  return app.patch(`api/${url}`, data);
}
