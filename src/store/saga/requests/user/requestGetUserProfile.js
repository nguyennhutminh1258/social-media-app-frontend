import app from "../../../../utils/axiosConfig";

export function requestGetUserProfile(payload) {
  const { url, id } = payload;
  
  return app.get(`api/${url}/${id}`);
}
