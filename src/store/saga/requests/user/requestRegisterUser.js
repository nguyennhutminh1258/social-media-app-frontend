import app from "../../../../utils/axiosConfig";

export function requestRegisterUser(payload) {
  const { url, data } = payload;
  
  return app.post(`api/${url}`, data);
}
