import app from "../../../../utils/axiosConfig";

export function requestSearchUser(payload) {
  const { url, search } = payload;
  
  return app.get(`api/${url}${search}`);
}
