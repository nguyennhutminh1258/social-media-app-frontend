import app from "../../../../utils/axiosConfig";

export function requestRefreshToken(payload) {
  const { url } = payload;
  
  return app.post(`api/${url}`);
}
