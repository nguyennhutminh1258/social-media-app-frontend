import app from "../../../../utils/axiosConfig";

export function requestGetUser(payload) {
  const { url, id } = payload;
  
  return app.get(`api/${url}/${id}`);
}
