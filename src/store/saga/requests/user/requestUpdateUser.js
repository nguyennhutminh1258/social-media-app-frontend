import app from "../../../../utils/axiosConfig";

export function requestUpdateUser(payload) {
  const { url, data } = payload;
  
  return app.patch(`api/${url}`, data);
}
