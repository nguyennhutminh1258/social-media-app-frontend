import app from "../../../../utils/axiosConfig";

export function requestUnfollow(payload) {
  const { url, data } = payload;
  
  return app.patch(`api/${url}`, data);
}
