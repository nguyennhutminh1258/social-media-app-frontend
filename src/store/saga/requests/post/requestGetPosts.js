import app from "../../../../utils/axiosConfig";

export function requestGetPosts(payload) {
  const { url, id, skip, limit } = payload;
  
  return app.get(`api/${url}/${id}?skip=${skip}&limit=${limit}`);
}
