import app from "../../../../utils/axiosConfig";

export function requestUpdatePost(payload) {
  const { url, data } = payload;
  
  return app.patch(`api/${url}/${data._idPost}`, data);
}
