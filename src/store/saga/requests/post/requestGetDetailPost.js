import app from "../../../../utils/axiosConfig";

export function requestGetDetailPost(payload) {
  const { url, id } = payload;
  
  return app.get(`api/${url}/${id}`);
}
