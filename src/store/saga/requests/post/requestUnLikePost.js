import app from "../../../../utils/axiosConfig";

export function requestUnLikePost(payload) {
  const { url, data } = payload;
  
  return app.patch(`api/${url}/${data.post._id}/unlike`, {
    idUser: data.userInfo._id,
  });
}
