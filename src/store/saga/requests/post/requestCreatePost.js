import app from "../../../../utils/axiosConfig";

export function requestCreatePost(payload) {
  const { url, data } = payload;
  
  return app.post(`api/${url}`, data);
}
