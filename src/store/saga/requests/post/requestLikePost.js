import app from "../../../../utils/axiosConfig";

export function requestLikePost(payload) {
  const { url, data } = payload;
  
  return app.patch(`api/${url}/${data.post._id}/like`, {
    idUser: data.userInfo._id,
  });
}
