import app from "../../../../utils/axiosConfig";

export function requestDeletePost(payload) {
  const { url, data } = payload;
  
  return app.delete(`api/${url}/${data.idPost}`, {
    data: { idUserPost: data.idUserPost },
  });
}
