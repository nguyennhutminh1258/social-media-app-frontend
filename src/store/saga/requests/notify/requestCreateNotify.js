import app from "../../../../utils/axiosConfig";

export function requestCreateNotify(payload) {
  const { url, data } = payload;
  
  return app.post(`api/${url}`, data);
}
