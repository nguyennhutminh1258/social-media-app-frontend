import app from "../../../../utils/axiosConfig";

export function requestReadNotify(payload) {
  const { url, id } = payload;
  
  return app.patch(`api/${url}/${id}`);
}
