import app from "../../../../utils/axiosConfig";

export function requestGetNotifies(payload) {
  const { url, id } = payload;
  
  return app.get(`api/${url}/${id}`);
}
