import app from "../../../../utils/axiosConfig";

export function requestRemoveNotify(payload) {
  const { url, data } = payload;
  
  return app.delete(`api/${url}/${data.id}?url=${data.url}`);
}
