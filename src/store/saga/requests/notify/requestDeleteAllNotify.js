import app from "../../../../utils/axiosConfig";

export function requestDeleteAllNotify(payload) {
  const { url, id } = payload;
  
  return app.delete(`api/${url}/${id}`);
}
