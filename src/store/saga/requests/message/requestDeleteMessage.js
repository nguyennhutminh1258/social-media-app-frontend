import app from "../../../../utils/axiosConfig";

export function requestDeleteMessage(payload) {
  const { url, data } = payload;

  return app.delete(`api/${url}/${data.idMessage}`, {
    data: { idSender: data.idSender, idRecipient: data.idRecipient },
  });
}
