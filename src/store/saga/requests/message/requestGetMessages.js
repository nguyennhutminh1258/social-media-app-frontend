import app from "../../../../utils/axiosConfig";

export function requestGetMessages(payload) {
  const { url, data } = payload;

  return app.get(`api/${url}/${data.sender}/${data.recipient}`);
}
