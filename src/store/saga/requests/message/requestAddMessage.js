import app from "../../../../utils/axiosConfig";

export function requestAddMessage(payload) {
  const { url, data } = payload;

  return app.post(`api/${url}`, data);
}
