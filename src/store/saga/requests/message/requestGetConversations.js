import app from "../../../../utils/axiosConfig";

export function requestGetConversations(payload) {
  const { url, id } = payload;
  
  return app.get(`api/${url}/${id}`);
}
