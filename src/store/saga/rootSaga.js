import { takeLatest } from "redux-saga/effects";
import {
  createComment,
  deleteComment,
  likeComment,
  unLikeComment,
  updateComment,
} from "../slice/commentSlice";
import {
  addMessage,
  deleteMessage,
  getConversations,
  getMessages,
  searchUserMess,
} from "../slice/messageSlice";
import {
  createNotify,
  deleteAllNotify,
  getNotifies,
  readNotify,
  removeNotify,
} from "../slice/notifySlice";
import {
  createPost,
  deletePost,
  getPosts,
  likePost,
  unLikePost,
  updatePost,
} from "../slice/postSlice";
import {
  follow,
  getUserPost,
  getUserProfile,
  unfollow,
} from "../slice/profileSlice";
import { searchUser } from "../slice/searchSlice";
import {
  changePassword,
  getUserInfo,
  login,
  logout,
  refreshToken,
  registerUser,
  updateUser,
} from "../slice/userSlice";
//Post
import { getDetailPost } from "../slice/detailPostSlice";
import { handleCreatePost } from "./handlers/post/handleCreatePost";
import { handleDeletePost } from "./handlers/post/handleDeletePost";
import { handleGetDetailPost } from "./handlers/post/handleGetDetailPost";
import { handleGetPosts } from "./handlers/post/handleGetPosts";
import { handleLikePost } from "./handlers/post/handleLikePost";
import { handleUnLikePost } from "./handlers/post/handleUnLikePost";
import { handleUpdatePost } from "./handlers/post/handleUpdatePost";
import { handleGetUserPost } from "./handlers/user/handleGetUserPost";
//User
import { handleChangePassword } from "./handlers/user/handleChangePassword";
import { handleFollow } from "./handlers/user/handleFollow";
import { handleGetUser } from "./handlers/user/handleGetUser";
import { handleGetUserProfile } from "./handlers/user/handleGetUserProfile";
import { handleLogin } from "./handlers/user/handleLogin";
import { handleLogout } from "./handlers/user/handleLogout";
import { handleRefreshToken } from "./handlers/user/handleRefreshToken";
import { handleRegisterUser } from "./handlers/user/handleRegisterUser";
import { handleSearchUser } from "./handlers/user/handleSearchUser";
import { handleUnfollow } from "./handlers/user/handleUnfollow";
import { handleUpdateUser } from "./handlers/user/handleUpdateUser";
//Comment
import { getUserSuggestion } from "../slice/suggestionSlice";
import { handleCreateComment } from "./handlers/comment/handleCreateComment";
import { handleDeleteComment } from "./handlers/comment/handleDeleteComment";
import { handleLikeComment } from "./handlers/comment/handleLikeComment";
import { handleUnLikeComment } from "./handlers/comment/handleUnLikeComment";
import { handleUpdateComment } from "./handlers/comment/handleUpdateComment";
import { handleGetSuggestion } from "./handlers/user/handleGetSuggestion";
//Notify
import { handleCreateNotify } from "./handlers/notify/handleCreateNotify";
import { handleDeleteAllNotify } from "./handlers/notify/handleDeleteAllNotify";
import { handleGetNotifies } from "./handlers/notify/handleGetNotifies";
import { handleReadNotify } from "./handlers/notify/handleReadNotify";
import { handleRemoveNotify } from "./handlers/notify/handleRemoveNotify";
// Message
import { handleAddMessage } from "./handlers/message/handleAddMessage";
import { handleGetConversations } from "./handlers/message/handleGetConversations";
import { handleGetMessages } from "./handlers/message/handleGetMessages";
import { handleSearchUserMess } from "./handlers/message/handleSearchUserMess";
import { handleDeleteMessage } from "./handlers/message/handleDeleteMessage";

export function* watcherSaga() {
  //User action
  yield takeLatest(login.type, handleLogin);
  yield takeLatest(logout.type, handleLogout);
  yield takeLatest(registerUser.type, handleRegisterUser);
  yield takeLatest(refreshToken.type, handleRefreshToken);
  yield takeLatest(searchUser.type, handleSearchUser);
  yield takeLatest(changePassword.type, handleChangePassword);

  //Profile action
  yield takeLatest(getUserInfo.type, handleGetUser);
  yield takeLatest(getUserProfile.type, handleGetUserProfile);
  yield takeLatest(getUserPost.type, handleGetUserPost);
  yield takeLatest(updateUser.type, handleUpdateUser);
  yield takeLatest(follow.type, handleFollow);
  yield takeLatest(unfollow.type, handleUnfollow);

  // Get user suggestion
  yield takeLatest(getUserSuggestion.type, handleGetSuggestion);

  //Post action
  yield takeLatest(createPost.type, handleCreatePost);
  yield takeLatest(getPosts.type, handleGetPosts);
  yield takeLatest(getDetailPost.type, handleGetDetailPost);
  yield takeLatest(updatePost.type, handleUpdatePost);
  yield takeLatest(deletePost.type, handleDeletePost);
  yield takeLatest(likePost.type, handleLikePost);
  yield takeLatest(unLikePost.type, handleUnLikePost);

  //Comment action
  yield takeLatest(createComment.type, handleCreateComment);
  yield takeLatest(updateComment.type, handleUpdateComment);
  yield takeLatest(deleteComment.type, handleDeleteComment);
  yield takeLatest(likeComment.type, handleLikeComment);
  yield takeLatest(unLikeComment.type, handleUnLikeComment);

  //Notify action
  yield takeLatest(createNotify.type, handleCreateNotify);
  yield takeLatest(removeNotify.type, handleRemoveNotify);
  yield takeLatest(getNotifies.type, handleGetNotifies);
  yield takeLatest(readNotify.type, handleReadNotify);
  yield takeLatest(deleteAllNotify.type, handleDeleteAllNotify);

  //Message action
  yield takeLatest(searchUserMess.type, handleSearchUserMess);
  yield takeLatest(addMessage.type, handleAddMessage);
  yield takeLatest(getConversations.type, handleGetConversations);
  yield takeLatest(getMessages.type, handleGetMessages);
  yield takeLatest(deleteMessage.type, handleDeleteMessage);
}
