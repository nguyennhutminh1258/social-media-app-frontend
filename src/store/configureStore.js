import { combineReducers, configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { watcherSaga } from "./saga/rootSaga";

import commentSlice from "./slice/commentSlice";
import detailPostSlice from "./slice/detailPostSlice";
import alertSlice from "./slice/alertSlice";
import postSlice from "./slice/postSlice";
import profileSlice from "./slice/profileSlice";
import searchSlice from "./slice/searchSlice";
import socketSlice from "./slice/socketSlice";
import statusSlice from "./slice/statusSlice";
import suggestionSlice from "./slice/suggestionSlice";
import userSlice from "./slice/userSlice";
import notifySlice from "./slice/notifySlice";
import messageSlice from "./slice/messageSlice";

const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];

const reducer = combineReducers({
  user: userSlice,
  profile: profileSlice,
  post: postSlice,
  detailPost: detailPostSlice,
  alert: alertSlice,
  notify: notifySlice,
  search: searchSlice,
  status: statusSlice,
  comment: commentSlice,
  suggestion: suggestionSlice,
  socket: socketSlice,
  message: messageSlice,
});

const store = configureStore({ reducer, middleware });

sagaMiddleware.run(watcherSaga);

export default store;
