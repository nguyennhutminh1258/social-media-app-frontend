import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { setUpdatedDetailPost } from "./store/slice/detailPostSlice";
import {
  addMessage,
  setMessages,
  setUserList,
} from "./store/slice/messageSlice";
import { setNotify } from "./store/slice/notifySlice";
import { setUpdatedPosts } from "./store/slice/postSlice";
import {
  setUpdatedUserPosts,
  setUserProfile,
} from "./store/slice/profileSlice";
import { setUserInfo } from "./store/slice/userSlice";

const SocketClient = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const userInfo = useSelector((state) => state.user.userInfo);
  const userProfile = useSelector((state) => state.profile.userProfile);
  const socket = useSelector((state) => state.socket);
  const notifies = useSelector((state) => state.notify);
  const messages = useSelector((state) => state.message.data);

  //joinUser
  useEffect(() => {
    socket.emit("joinUser", userInfo._id);
  }, [socket, userInfo]);

  //like
  useEffect(() => {
    socket.on("likeToClient", (newPost) => {
      dispatch(setUpdatedPosts(newPost));
      dispatch(setUpdatedUserPosts(newPost));
      dispatch(setUpdatedDetailPost(newPost));
    });

    return () => socket.off("likeToClient");
  }, [dispatch, socket]);

  //unLike
  useEffect(() => {
    socket.on("unLikeToClient", (newPost) => {
      dispatch(setUpdatedPosts(newPost));
      dispatch(setUpdatedUserPosts(newPost));
      dispatch(setUpdatedDetailPost(newPost));
    });

    return () => socket.off("unLikeToClient");
  }, [dispatch, socket]);

  // create comment
  useEffect(() => {
    socket.on("createCommentToClient", (newPostAfter) => {
      dispatch(setUpdatedPosts(newPostAfter));
      dispatch(setUpdatedUserPosts(newPostAfter));
      dispatch(setUpdatedDetailPost(newPostAfter));
    });

    return () => socket.off("createCommentToClient");
  }, [dispatch, socket]);

  // delete comment
  useEffect(() => {
    socket.on("deleteCommentToClient", (newPost) => {
      dispatch(setUpdatedPosts(newPost));
      dispatch(setUpdatedUserPosts(newPost));
      dispatch(setUpdatedDetailPost(newPost));
    });

    return () => socket.off("deleteCommentToClient");
  }, [dispatch, socket]);

  // follow
  useEffect(() => {
    socket.on("followToClient", (newUser) => {
      if (Object.keys(userProfile).length > 0) {
        dispatch(setUserProfile(newUser));
      }
      dispatch(setUserInfo(newUser));
    });

    return () => socket.off("followToClient");
  }, [dispatch, socket, userProfile]);

  // unFollow
  useEffect(() => {
    socket.on("unFollowToClient", (newUser) => {
      if (Object.keys(userProfile).length > 0) {
        dispatch(setUserProfile(newUser));
      }
      dispatch(setUserInfo(newUser));
    });

    return () => socket.off("unFollowToClient");
  }, [dispatch, socket, userProfile]);

  //Create Notification
  useEffect(() => {
    socket.on("createNotifyToClient", (notify) => {
      const newNotifies = [notify, ...notifies];
      dispatch(setNotify(newNotifies));
    });

    return () => socket.off("createNotifyToClient");
  }, [dispatch, notifies, socket]);

  //Remove Notification
  useEffect(() => {
    socket.on("removeNotifyToClient", (notify) => {
      const newNotifies = notifies.filter(
        (item) => item.id !== notify.id || item.url !== notify.url
      );
      dispatch(setNotify(newNotifies));
    });

    return () => socket.off("removeNotifyToClient");
  }, [dispatch, notifies, socket]);

  // Add Message
  useEffect(() => {
    socket.on("addMessageToClient", (message) => {
      if (message.sender === id) {
        dispatch(addMessage(message));
      } else {
        // dispatch(setData(message));
        dispatch(setUserList(message));
      }
    });

    return () => socket.off("addMessageToClient");
  }, [dispatch, id, socket]);

  // Delete Message
  useEffect(() => {
    socket.on("deleteMessageToClient", (message) => {
      const newMessages = messages.filter((item) => item._id !== message._id);
      dispatch(setMessages(newMessages));
    });

    return () => socket.off("deleteMessageToClient");
  }, [dispatch, id, messages, socket, userInfo._id]);

  return <></>;
};

export default SocketClient;
