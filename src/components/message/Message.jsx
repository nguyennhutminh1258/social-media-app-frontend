import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import {
  Avatar,
  IconButton,
  Menu,
  MenuItem,
  Stack,
  Typography,
  useTheme,
} from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { deleteMessage } from "../../store/slice/messageSlice";

const Message = ({ user, owner, message }) => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const [showMoreIcon, setShowMoreIcon] = useState(false);

  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleShowMoreIcon = () => {
    setShowMoreIcon(true);
  };

  const handleHideMoreIcon = () => {
    setShowMoreIcon(false);
  };

  const handleDeleteMessage = () => {
    dispatch(deleteMessage(message));
  };

  return (
    <Stack
      direction={owner ? "row-reverse" : "row"}
      alignSelf={owner ? "flex-end" : "stretch"}
      spacing={1}
      sx={{ maxWidth: "70%" }}
      onMouseOver={handleShowMoreIcon}
      onMouseOut={handleHideMoreIcon}
    >
      <Stack>
        <Avatar
          sx={{ width: 45, height: 45 }}
          alt="avatar-user"
          src={user?.avatar}
        />
      </Stack>
      <Stack>
        <Typography
          alignSelf={owner ? "flex-end" : "stretch"}
          component="span"
          fontSize={10}
          fontWeight={300}
        >
          {new Date(message.createdAt).toLocaleString()}
        </Typography>
        {message.text && (
          <Stack
            sx={{
              backgroundColor:
                theme.palette.mode === "dark" ? "#404040" : "#f0f0f0",
              padding: 1,
              borderRadius: 3,
            }}
          >
            <Typography component="span" fontSize={13} fontWeight={400}>
              {message.text}
            </Typography>
          </Stack>
        )}
        {message.media.map((item) => (
          <Stack key={item.url} sx={{ marginTop: 1 }}>
            <img
              src={item.url}
              style={{ width: "100%", height: "100%", objectFit: "contain" }}
              alt="images upload"
            />
          </Stack>
        ))}
      </Stack>
      {owner && showMoreIcon && (
        <Stack alignItems="center" justifyContent="center">
          <IconButton
            size="small"
            disableRipple
            disableFocusRipple
            onClick={handleClick}
          >
            <MoreVertIcon />
          </IconButton>

          <Menu
            sx={{ marginTop: 7 }}
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
            transformOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
          >
            <MenuItem onClick={handleDeleteMessage}>
              <Stack direction="row" spacing={1}>
                <DeleteOutlineIcon size="small" />
                <Typography component={"span"} fontSize={13} fontWeight={400}>
                  Xóa tin nhắn
                </Typography>
              </Stack>
            </MenuItem>
          </Menu>
        </Stack>
      )}
    </Stack>
  );
};

export default Message;
