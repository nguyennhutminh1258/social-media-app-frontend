import { Stack } from "@mui/system";
import { useSelector } from "react-redux";
import PostCard from "../post-card/PostCard";

const Posts = () => {
  const posts = useSelector((state) => state.post.posts);

  return (
    <Stack
      spacing={2}
      alignItems="center"
      sx={{ width: "100%", overflow: "auto" }}
    >
      {posts?.map((post) => {
        return <PostCard post={post} key={post._id} />;
      })}
    </Stack>
  );
};

export default Posts;
