import { Avatar, ListItem, ListItemAvatar, ListItemText, Typography } from "@mui/material";
import { memo } from "react";
import ImageIcon from "@mui/icons-material/Image";

const UserCard = ({ user, notDevider, hasMessage }) => {
  return (
    <ListItem
      alignItems="flex-start"
      key={user?._id}
      divider={!notDevider}
      sx={{ cursor: "pointer" }}>
      <ListItemAvatar>
        <Avatar alt="avatar-user" src={user?.avatar} />
      </ListItemAvatar>
      <ListItemText
        primary={
          <>
            <Typography sx={{ display: "inline" }} component="span" variant="body2" color="text.primary">
              {user?.name}
            </Typography>
          </>
        }
        secondary={
          <>
            <Typography sx={{ display: "inline" }} component="span" variant="body2" color="text.secondary">
              {hasMessage ? (
                <>
                  {user.text}
                  {user.media?.length > 0 && (
                    <>
                      <br />
                      {user.media.length}
                      <ImageIcon style={{ position: "relative", top: 5 }} />
                    </>
                  )}
                </>
              ) : (
                user?.userName
              )}
            </Typography>
          </>
        }
      />
    </ListItem>
  );
};

export default memo(UserCard);
