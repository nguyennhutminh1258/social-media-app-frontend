import { useEffect, useRef, useState } from "react";

const LazyImage = ({ src, index, i, detail }) => {
  const [inView, setInView] = useState(false);
  const imageRef = useRef();

  useEffect(() => {
    let callback = (entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          setInView(true);
        }
      });
    };
    let observer = new IntersectionObserver(callback);

    if (imageRef?.current) {
      observer.observe(imageRef.current);
    }

    return () => {
      observer.disconnect();
    };
  }, [imageRef]);

  return inView ? (
    <img
      id={src}
      src={src}
      alt="status"
      style={{
        display: index === i ? "block" : "none",
        width: "100%",
        objectFit: detail ? "contain" : "cover",
        aspectRatio: "16/9",
      }}
    />
  ) : (
    <img
      id={src}
      ref={imageRef}
      alt="status"
      style={{
        width: "100%",
        backgroundColor: "#ddd",
      }}
    />
  );
};

export default LazyImage;
