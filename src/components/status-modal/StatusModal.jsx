import CloseIcon from "@mui/icons-material/Close";
import PhotoCameraOutlinedIcon from "@mui/icons-material/PhotoCameraOutlined";
import PhotoSizeSelectActualOutlinedIcon from "@mui/icons-material/PhotoSizeSelectActualOutlined";
import { IconButton, ImageList, ImageListItem, Paper, Tooltip, Typography } from "@mui/material";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import { Stack } from "@mui/system";
import { useCallback, useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { setMessageError } from "../../store/slice/alertSlice";
import { createPost, setOnEdit, updatePost } from "../../store/slice/postSlice";
import { setShowStatus } from "../../store/slice/statusSlice";
import SentimentVerySatisfiedIcon from "@mui/icons-material/SentimentVerySatisfied";
import EmojiMenu from "../emoji-menu/EmojiMenu";

const StatusModal = () => {
  const dispatch = useDispatch();

  const userInfo = useSelector((state) => state.user.userInfo);
  const showStatus = useSelector((state) => state.status.showStatus);
  const postEdit = useSelector((state) => state.status.postEdit);
  const onEdit = useSelector((state) => state.post.onEdit);

  // Danh sách hình ảnh cho bài viết
  const [images, setImages] = useState([]);
  // Hiện danh sách hình ảnh
  const [showImageList, setShowImageList] = useState(false);
  // Hiện camera
  const [stream, setStream] = useState(false);
  const [tracks, setTracks] = useState("");
  // Hiện menu emoji
  const [anchorEmoji, setAnchorEmoji] = useState(null);
  const openEmoji = Boolean(anchorEmoji);

  const videoRef = useRef();
  const canvasRef = useRef();

  const {
    register,
    handleSubmit,
    setValue,
    getValues,
    reset,
    formState: { errors },
  } = useForm();

  const handleOpenEmoji = (event) => {
    setAnchorEmoji(event.currentTarget);
  };
  const handleCloseEmoji = () => {
    setAnchorEmoji(null);
  };

  const handleClose = () => {
    dispatch(setOnEdit(false));
    dispatch(setShowStatus(false));
  };

  const handleChooseImage = (e) => {
    // Chọn ảnh từ thiết bị
    const files = [...e.target.files];

    let newImages = [];
    files.forEach((file) => {
      if (!file) {
        return dispatch(setMessageError("File không tồn tại."));
      } else if (file.type !== "image/jpeg" && file.type !== "image/png") {
        return dispatch(setMessageError("Định dạng file không đúng"));
      } else {
        return newImages.push(file);
      }
    });

    setImages([...images, ...newImages]);
  };

  const removeImage = (index) => {
    // Xóa ảnh khỏi danh sách ảnh bài viết
    const newImages = [...images];
    newImages.splice(index, 1);
    setImages(newImages);
  };

  const handleStream = () => {
    // Bật camera
    setStream(true);
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices
        .getUserMedia({ video: true })
        .then((mediaStream) => {
          videoRef.current.srcObject = mediaStream;
          videoRef.current.play();
          const track = mediaStream.getTracks();
          setTracks(track[0]);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const handleStopStream = useCallback(() => {
    // Dừng chế độ chụp ảnh
    tracks.stop();
    setStream(false);
  }, [tracks]);

  const handleCapture = () => {
    // Chụp ảnh qua camera
    const width = videoRef.current.clientWidth;
    const height = videoRef.current.clientHeight;

    canvasRef.current.setAttribute("width", width);
    canvasRef.current.setAttribute("height", height);

    const ctx = canvasRef.current.getContext("2d");
    ctx.drawImage(videoRef.current, 0, 0, width, height);
    let URL = canvasRef.current.toDataURL();

    setImages([...images, { camera: URL }]);
  };

  const handleOnSubmit = (data) => {
    // kiểm tra xem người dùng đã thêm ảnh vào chưa
    if (data.images.length === 0) {
      dispatch(setMessageError("Hãy thêm ít nhất 1 ảnh vào bài viết"));
    } else if (onEdit) {
      dispatch(updatePost(data));
    } else {
      dispatch(createPost(data));
    }
  };

  useEffect(() => {
    // Clear data in form when modal is closed
    if (!showStatus && !onEdit) {
      if (tracks) {
        handleStopStream();
      }
      setImages([]);
      reset();
    }
  }, [handleStopStream, onEdit, reset, showStatus, tracks]);

  useEffect(() => {
    setShowImageList(images.length !== 0);
  }, [images.length]);

  useEffect(() => {
    // Nếu đang ở mode edit post thì sẽ hiện nội dung bài viết
    if (onEdit) {
      setValue("_idPost", postEdit._id);
      setValue("content", postEdit.content);
    } else {
      setValue("_id", userInfo._id);
    }
    setValue("images", images);
  }, [images, onEdit, postEdit._id, postEdit.content, setValue, userInfo._id]);

  useEffect(() => {
    if (onEdit) {
      setImages(postEdit.images);
    }
  }, [onEdit, postEdit.images]);

  return (
    <Dialog open={showStatus || onEdit} onClose={handleClose} fullWidth={true} maxWidth={"sm"}>
      <form onSubmit={handleSubmit(handleOnSubmit)}>
        <DialogTitle>
          <Typography textAlign={"center"} fontSize={20} fontWeight={600}>
            {onEdit ? "Sửa bài viết" : "Tạo bài viết"}
          </Typography>
          <IconButton
            aria-label="close"
            onClick={handleClose}
            sx={{
              position: "absolute",
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}>
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers={true}>
          <TextField
            margin="dense"
            label="Bạn đang nghĩ gì thế?"
            type="text"
            fullWidth
            multiline
            rows={5}
            variant="standard"
            InputProps={{
              disableUnderline: true,
            }}
            {...register("content", { required: "Vui lòng nhập nội dung status" })}
            error={errors.content ? true : false}
            helperText={errors.content && errors.content.message}
          />

          <IconButton onClick={handleOpenEmoji}>
            <SentimentVerySatisfiedIcon />
          </IconButton>

          {showImageList && (
            <ImageList sx={{ width: "100%", height: 200 }} cols={3}>
              {images.map((item, index) => (
                <ImageListItem key={index}>
                  <img
                    cols={1}
                    rows={1}
                    src={item.camera ? item.camera : item.url ? item.url : URL.createObjectURL(item)}
                    alt="images"
                    sx={{ objectFit: "contain" }}
                  />
                  <IconButton
                    aria-label="close"
                    onClick={() => removeImage(index)}
                    sx={{
                      position: "absolute",
                      right: 4,
                      top: 4,
                      color: (theme) => theme.palette.grey[900],
                    }}>
                    <CloseIcon />
                  </IconButton>
                </ImageListItem>
              ))}
            </ImageList>
          )}

          {stream && (
            <Paper elevation={0} sx={{ padding: "8px 0" }}>
              <Stack sx={{ position: "relative" }} alignItems="center">
                <video autoPlay muted ref={videoRef} style={{ maxHeight: "300px", maxWidth: "350px" }} />
                <IconButton
                  aria-label="close"
                  onClick={handleStopStream}
                  sx={{
                    position: "absolute",
                    right: 100,
                    top: 4,
                    color: (theme) => theme.palette.grey[900],
                  }}>
                  <CloseIcon />
                </IconButton>
                <canvas ref={canvasRef} style={{ display: "none" }} />
              </Stack>
            </Paper>
          )}
        </DialogContent>
        <DialogActions sx={{ width: "100%" }}>
          {stream ? (
            <Stack sx={{ width: "100%" }} alignItems="center" spacing={2}>
              <IconButton component="label" sx={{ color: "inherit" }} size="large" onClick={handleCapture}>
                <PhotoCameraOutlinedIcon />
              </IconButton>
            </Stack>
          ) : (
            <Stack direction="column" sx={{ width: "100%" }} justifyContent="center" spacing={2}>
              <Stack
                direction="row"
                justifyContent="center"
                spacing={2}
                sx={{ width: "100%", textTransform: "none", textAlign: "center", outline: "1px solid grey" }}>
                <Tooltip title="Thêm ảnh từ camera">
                  <IconButton component="label" sx={{ color: "inherit" }} size="large" onClick={handleStream}>
                    <PhotoCameraOutlinedIcon />
                  </IconButton>
                </Tooltip>
                <Tooltip title="Thêm ảnh từ file có sẵn">
                  <IconButton
                    aria-label="upload picture"
                    component="label"
                    sx={{ color: "inherit" }}
                    size="large">
                    <input
                      hidden
                      multiple
                      accept="image/*"
                      type="file"
                      onChange={(e) => handleChooseImage(e)}
                    />
                    <PhotoSizeSelectActualOutlinedIcon />
                  </IconButton>
                </Tooltip>
              </Stack>
              <Button type="submit" variant="contained" sx={{ width: "100%" }}>
                {onEdit ? "Sửa" : "Đăng"}
              </Button>
            </Stack>
          )}
        </DialogActions>
      </form>
      <EmojiMenu
        anchorEmoji={anchorEmoji}
        openEmoji={openEmoji}
        handleCloseEmoji={handleCloseEmoji}
        setValue={setValue}
        getValues={getValues}
      />
    </Dialog>
  );
};

export default StatusModal;
