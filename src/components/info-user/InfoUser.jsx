import { Avatar, Button, Divider, Paper, Typography } from "@mui/material";
import { Box, Stack } from "@mui/system";
import { useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

import EditProfile from "../edit-profile/EditProfile";
import FollowBtn from "../follow-btn/FollowBtn";
import Followers from "../followers/Followers";
import Following from "../following/Following";

const InfoUser = () => {
  const { id } = useParams();
  // Thông tin người đăng nhập
  const userInfo = useSelector((state) => state.user.userInfo);
  // Thông tin người dùng trang profile
  const userProfile = useSelector((state) => state.profile.userProfile);
  const posts = useSelector((state) => state.profile.posts);
  // Mở modal sửa thông tin người dùng
  const [openEdit, setOpenEdit] = useState(false);
  // Mở modal người theo dõi
  const [openFollowers, setOpenFollowers] = useState(false);
  // Mở modal người đang theo dõi
  const [openFollowing, setOpenFollowing] = useState(false);

  const handleClickOpenEdit = () => {
    setOpenEdit(true);
  };

  const handleCloseEdit = () => {
    setOpenEdit(false);
  };

  const handleOpenFollowers = () => {
    setOpenFollowers(true);
  };

  const handleCloseFollowers = () => {
    setOpenFollowers(false);
  };

  const handleOpenFollowing = () => {
    setOpenFollowing(true);
  };

  const handleCloseFollowing = () => {
    setOpenFollowing(false);
  };

  return (
    <Paper elevation={2}>
      <Stack
        direction="row"
        justifyContent="space-around"
        spacing={2}
        sx={{ marginTop: 4, padding: 3 }}
      >
        <Box>
          <Box>
            <Avatar
              sx={{
                marginRight: { md: 4, sm: 0, xs: 0 },
                height: { md: 160, sm: 120, xs: 80 },
                width: { md: 160, sm: 120, xs: 80 },
              }}
              alt="avatar-user"
              src={userProfile?.avatar}
            />
          </Box>
        </Box>
        <Stack direction="column" spacing={2}>
          <Stack
            direction={{ md: "row", sm: "row", xs: "column" }}
            justifyContent="space-around"
            alignItems="center"
            spacing={2}
          >
            <Box>
              <Typography
                component="h4"
                fontSize={{ md: 24, sm: 20, xs: 18 }}
                sx={{
                  textOverflow: "ellipsis",
                  overflow: "hidden",
                  WebkitLineClamp: "1",
                  WebkitBoxOrient: "vertical",
                  whiteSpace: "nowrap",
                  "@media (max-width: 350px)": {
                    maxWidth: "100px",
                  },
                }}
              >
                {userProfile?.userName}
              </Typography>
            </Box>
            {id === userInfo._id ? (
              <Box>
                <Button
                  variant="outlined"
                  size="small"
                  sx={{ fontSize: 12, borderRadius: 20 }}
                  onClick={handleClickOpenEdit}
                >
                  Chỉnh sửa
                </Button>
              </Box>
            ) : (
              <FollowBtn userProfile={userProfile} />
            )}
          </Stack>
          <Stack
            display={{ md: "flex", sm: "flex", xs: "none" }}
            direction="row"
            alignItems="center"
            spacing={5}
          >
            <Box sx={{ textAlign: "center" }}>
              <Typography
                component="span"
                fontSize={{ md: 15, sm: 15 }}
                textAlign="center"
              >
                <Typography
                  component="span"
                  fontSize={{ md: 20, sm: 15 }}
                  fontWeight={600}
                  display={"block"}
                  textAlign="center"
                >
                  {posts?.length}
                </Typography>
                bài viết
              </Typography>
            </Box>
            <Box sx={{ textAlign: "center" }}>
              <Typography
                component="span"
                fontSize={{ md: 15, sm: 15 }}
                textAlign="center"
                sx={{ cursor: "pointer" }}
                onClick={handleOpenFollowers}
              >
                <Typography
                  component="span"
                  fontSize={{ md: 20, sm: 15 }}
                  fontWeight={600}
                  display={"block"}
                  textAlign="center"
                >
                  {userProfile?.followers?.length}
                </Typography>
                người theo dõi
              </Typography>
            </Box>
            <Box sx={{ textAlign: "center" }}>
              <Typography
                component="span"
                fontSize={{ md: 15, sm: 15 }}
                textAlign="center"
                sx={{ cursor: "pointer" }}
                onClick={handleOpenFollowing}
              >
                <Typography
                  component="span"
                  fcomponent="span"
                  fontSize={{ md: 20, sm: 15 }}
                  fontWeight={600}
                  display={"block"}
                  textAlign="center"
                >
                  {userProfile?.following?.length}
                </Typography>
                người đang theo dõi
              </Typography>
            </Box>
          </Stack>
          <Stack
            display={{ md: "flex", xs: "none" }}
            direction="row"
            alignItems="center"
            spacing={2}
          >
            <Box>
              <Typography
                component="h3"
                variant="h3"
                sx={{ fontSize: "15px", fontWeight: "600" }}
              >
                {userProfile?.name}
              </Typography>
            </Box>
          </Stack>
          <Stack
            display={{ md: "flex", xs: "none" }}
            direction="row"
            alignItems="center"
          >
            <Box>
              <Typography component="p" sx={{ whiteSpace: "pre-line" }}>
                {userProfile?.description}
              </Typography>
            </Box>
          </Stack>
        </Stack>
      </Stack>
      <Stack display={{ md: "none", xs: "flex" }} padding={1} paddingLeft={7}>
        <Stack direction="row" alignItems="center" spacing={2}>
          <Box>
            <Typography
              component="h3"
              variant="h3"
              sx={{ fontSize: "15px", fontWeight: "600" }}
            >
              {userProfile?.name}
            </Typography>
          </Box>
        </Stack>
        <Stack direction="row" alignItems="center">
          <Box>
            <Typography
              component="p"
              fontSize={{ md: 20, sm: 15, xs: 12 }}
              fontWeight={400}
            >
              {userProfile?.description}
            </Typography>
          </Box>
        </Stack>
      </Stack>
      <Divider />
      <Stack
        display={{ md: "none", sm: "none", xs: "flex" }}
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        spacing={5}
        p={2}
      >
        <Box sx={{ textAlign: "center" }}>
          <Typography
            component="span"
            fontSize={{ md: 20, sm: 15, xs: 12 }}
            textAlign="center"
          >
            <Typography
              component="span"
              fontSize={{ md: 20, sm: 15 }}
              fontWeight={600}
              display={"block"}
              textAlign="center"
            >
              {posts?.length}
            </Typography>
            bài viết
          </Typography>
        </Box>
        <Box sx={{ textAlign: "center" }}>
          <Typography
            sx={{ cursor: "pointer" }}
            onClick={handleOpenFollowers}
            component="span"
            fontSize={{ md: 20, sm: 15, xs: 12 }}
            textAlign="center"
          >
            <Typography
              component="span"
              fontSize={{ md: 20, sm: 15 }}
              fontWeight={600}
              display={"block"}
              textAlign="center"
            >
              {userProfile?.followers?.length}
            </Typography>
            người theo dõi
          </Typography>
        </Box>
        <Box sx={{ textAlign: "center" }}>
          <Typography
            component="span"
            fontSize={{ md: 20, sm: 15, xs: 12 }}
            textAlign="center"
            sx={{ cursor: "pointer" }}
            onClick={handleOpenFollowing}
          >
            <Typography
              component="span"
              fcomponent="span"
              fontSize={{ md: 20, sm: 15 }}
              fontWeight={600}
              display={"block"}
              textAlign="center"
            >
              {userProfile?.following?.length}
            </Typography>
            người đang theo dõi
          </Typography>
        </Box>
      </Stack>
      <Divider />
      <EditProfile
        openEdit={openEdit}
        handleCloseEdit={handleCloseEdit}
        user={userInfo}
      />

      {openFollowers && (
        <Followers
          handleCloseFollowers={handleCloseFollowers}
          openFollowers={openFollowers}
          userProfile={userProfile}
        />
      )}

      {openFollowing && (
        <Following
          handleCloseFollowing={handleCloseFollowing}
          openFollowing={openFollowing}
          userProfile={userProfile}
        />
      )}
    </Paper>
  );
};

export default InfoUser;
