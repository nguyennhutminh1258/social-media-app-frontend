import { Button, Dialog, DialogActions, DialogTitle } from "@mui/material";
import React from "react";

const ConfirmDialog = ({ openConfirm, handleCloseConfirm, handleDeletePost }) => {
  return (
    <Dialog
      open={openConfirm}
      onClose={handleCloseConfirm}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description">
      <DialogTitle id="alert-dialog-title">Bạn có muốn xóa bài viết này không?</DialogTitle>
      <DialogActions>
        <Button onClick={handleCloseConfirm}>Hủy</Button>
        <Button color="success" onClick={handleDeletePost}>
          Đồng ý
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDialog;
