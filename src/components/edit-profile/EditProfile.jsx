import { Dialog, Tab, Tabs, useMediaQuery } from "@mui/material";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import EditPassWord from "./EditPassWord";
import EditProfileForm from "./EditProfileForm";

const EditProfile = ({ openEdit, handleCloseEdit, user }) => {
  const { id } = useParams();
  const minWidth750 = useMediaQuery("(min-width:750px)");

  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    if (id !== user._id) {
      handleCloseEdit();
    }
  }, [handleCloseEdit, id, user._id]);

  return (
    <Dialog
      fullWidth={true}
      maxWidth="sm"
      open={openEdit}
      onClose={handleCloseEdit}
      sx={{
        "& .MuiPaper-root": {
          display: "flex",
          flexDirection: `${minWidth750 ? "row" : "column"}`,
          height: 755,
        },
      }}>
      <Tabs
        orientation={minWidth750 ? "vertical" : "horizontal"}
        value={value}
        onChange={handleChange}
        sx={{
          borderRight: 1,
          borderColor: "divider",
          "& div.MuiTabs-scroller": {
            "& .MuiTabs-flexContainer": {
              justifyContent: `${minWidth750 ? "flex-start" : "space-around"}`,
            },
          },
        }}>
        <Tab label="Chỉnh sửa thông tin" />
        <Tab label="Đổi mật khẩu" />
      </Tabs>
      <div
        style={{ width: "100%" }}
        role="tabpanel"
        hidden={value !== 0}
        id={`vertical-tabpanel-${0}`}
        aria-labelledby={`vertical-tab-${0}`}>
        {value === 0 && <EditProfileForm handleCloseEdit={handleCloseEdit} user={user} />}
      </div>

      <div
        style={{ width: "100%" }}
        role="tabpanel"
        hidden={value !== 1}
        id={`vertical-tabpanel-${1}`}
        aria-labelledby={`vertical-tab-${1}`}>
        {value === 1 && <EditPassWord handleCloseEdit={handleCloseEdit} user={user} />}
      </div>
    </Dialog>
  );
};

export default EditProfile;
