import PhotoCamera from "@mui/icons-material/PhotoCamera";
import {
  Avatar,
  Box,
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  IconButton,
  InputLabel,
  Radio,
  RadioGroup,
  Stack,
  TextField,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { setMessageError } from "../../store/slice/alertSlice";
import { updateUser } from "../../store/slice/userSlice";

const EditProfileForm = ({ handleCloseEdit, user }) => {
  const dispatch = useDispatch();

  const [avatar, setAvatar] = useState("");
  const [showUploadBtn, setShowUploadBtn] = useState(false);

  const {
    register,
    handleSubmit,
    control,
    setValue,
    reset,
    formState: { errors },
  } = useForm();

  const handleChangeAvatar = (e) => {
    if (!e.target.files[0]) {
      return dispatch(setMessageError("File không tồn tại."));
    } else if (e.target.files[0].type !== "image/jpeg" && e.target.files[0].type !== "image/png") {
      return dispatch(setMessageError("Định dạng file không đúng"));
    } else {
      setAvatar(e.target.files[0]);
    }
  };

  const handleEdit = (data) => {
    dispatch(updateUser(data));
    handleCloseEdit();
  };

  useEffect(() => {
    setValue("gender", user.gender);
    setValue("avatar", avatar);
    setValue("_id", user._id);
  }, [setValue, user.gender, avatar, user._id]);

  return (
    <form onSubmit={handleSubmit(handleEdit)} style={{ padding: "20px 0" }}>
      <DialogTitle
        fontWeight="bold"
        fontSize="20px"
        letterSpacing="0.5px"
        lineHeight="16px"
        textAlign="center">
        Chỉnh sửa thông tin tài khoản
      </DialogTitle>
      <DialogContent>
        <Stack spacing={3} mt={2}>
          <Box
            sx={{
              position: "relative",
              margin: "0 auto",
            }}>
            <Avatar
              sx={{
                width: 100,
                height: 100,
                cursor: "pointer",
                WebkitFilter: `${showUploadBtn ? "brightness(60%)" : ""}`,
              }}
              src={avatar ? URL.createObjectURL(avatar) : user.avatar}
              onMouseOver={() => setShowUploadBtn(true)}
              onMouseOut={() => setShowUploadBtn(false)}
            />
            {showUploadBtn && (
              <IconButton
                color="warning"
                aria-label="upload picture"
                component="label"
                onMouseOver={() => setShowUploadBtn(true)}
                onMouseOut={() => setShowUploadBtn(false)}
                sx={{
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  transform: "translate(-50%, -50%)",
                }}>
                <input hidden accept="image/*" type="file" onChange={(e) => handleChangeAvatar(e)} />
                <PhotoCamera />
              </IconButton>
            )}
          </Box>
          <TextField
            label="Họ và tên"
            type="text"
            fullWidth
            variant="outlined"
            {...register("name", { required: "Vui lòng nhập họ và tên" })}
            error={!!errors.name}
            helperText={errors.name && errors.name.message}
            defaultValue={user?.name ?? null}
            size="small"
          />
          <TextField
            label="Tên tài khoản"
            type="text"
            fullWidth
            variant="outlined"
            {...register("userName", { required: "Vui lòng nhập tên tài khoản" })}
            error={!!errors.userName}
            helperText={errors.userName && errors.userName.message}
            defaultValue={user?.userName ?? null}
          />
          <TextField
            label="Địa chỉ email"
            type="text"
            fullWidth
            variant="outlined"
            {...register("email", {
              required: "Vui lòng nhập email",
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                message: "Vui lòng nhập email hợp lệ",
              },
            })}
            error={!!errors.email}
            helperText={errors.email && errors.email.message}
            defaultValue={user?.email ?? null}
            size="small"
          />
          <TextField
            name="description"
            label="Giới thiệu bản thân"
            {...register("description")}
            multiline
            rows={4}
            defaultValue={user?.description ?? null}
            variant="filled"
            size="small"
          />
          <Controller
            control={control}
            name="gender"
            render={({ field: { onChange, value } }) => (
              <>
                <InputLabel>Giới tính</InputLabel>
                <RadioGroup row value={value ?? null} onChange={onChange} style={{ marginTop: "0px" }}>
                  <FormControlLabel value="male" control={<Radio size="small" />} label="Nam" />
                  <FormControlLabel value="female" control={<Radio size="small" />} label="Nữ" />
                  <FormControlLabel value="other" control={<Radio size="small" />} label="Khác" />
                </RadioGroup>
              </>
            )}
          />
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            setAvatar("");
            reset();
            handleCloseEdit();
          }}
          variant="contained"
          color="warning"
          sx={{ borderRadius: 20 }}>
          Hủy
        </Button>
        <Button type="submit" variant="contained" color="success" sx={{ borderRadius: 20 }}>
          Lưu
        </Button>
      </DialogActions>
    </form>
  );
};

export default EditProfileForm;
