import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  InputAdornment,
  Stack,
  TextField,
} from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { changePassword } from "../../store/slice/userSlice";

const EditPassWord = ({ handleCloseEdit, user }) => {
  const dispatch = useDispatch();

  const [showOldPassword, setShowOldPassword] = useState(false);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [showConfirmPassword, setShowConfirmPassword] = useState(false);

  const {
    register,
    handleSubmit,
    reset,
    watch,
    setValue,
    formState: { errors },
  } = useForm();

  // Kiểm tra confirm password trùng với passwod chưa
  const password = useRef({});
  password.current = watch("newPassword", "");

  const handleClickShowOldPassword = () => {
    setShowOldPassword(!showOldPassword);
  };

  const handleClickShowNewPassword = () => {
    setShowNewPassword(!showNewPassword);
  };

  const handleClickShowConfirmPassword = () => {
    setShowConfirmPassword(!showConfirmPassword);
  };

  const handleChangePassword = (data) => {
    dispatch(changePassword(data))
      .then(handleCloseEdit)
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    setValue("_id", user._id);
  }, [setValue, user._id]);

  return (
    <form
      onSubmit={handleSubmit(handleChangePassword)}
      style={{ padding: "20px 0" }}
    >
      <DialogTitle
        fontWeight="bold"
        fontSize="20px"
        letterSpacing="0.5px"
        lineHeight="16px"
        textAlign="center"
      >
        Thay đổi mật khẩu
      </DialogTitle>
      <DialogContent>
        <Stack spacing={3} mt={2}>
          <TextField
            label="Mật khẩu cũ"
            type={showOldPassword ? "text" : "password"}
            fullWidth
            variant="outlined"
            {...register("oldPassword", {
              required: "Vui lòng nhập mật khẩu cũ",
            })}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    onClick={handleClickShowOldPassword}
                    aria-label="toggle password visibility"
                    edge="end"
                  >
                    {showOldPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
            error={!!errors.oldPassword}
            helperText={errors.oldPassword && errors.oldPassword.message}
            size="small"
          />
          <TextField
            label="Mật khẩu mới"
            type={showNewPassword ? "text" : "password"}
            fullWidth
            variant="outlined"
            {...register("newPassword", {
              required: "Vui lòng nhập mật khẩu mới",
              minLength: {
                value: 6,
                message: "Mật khẩu phải chứa ít nhất 6 kí tự",
              },
            })}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    onClick={handleClickShowNewPassword}
                    aria-label="toggle password visibility"
                    edge="end"
                  >
                    {showNewPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
            error={!!errors.newPassword}
            helperText={errors.newPassword && errors.newPassword.message}
            size="small"
          />
          <TextField
            label="Xác nhận mật khẩu mới"
            type={showConfirmPassword ? "text" : "password"}
            fullWidth
            variant="outlined"
            {...register("confirmPassword", {
              validate: (value) =>
                value === password.current ||
                "Mật khẩu không khớp nhau vui lòng nhập lại",
            })}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    onClick={handleClickShowConfirmPassword}
                    aria-label="toggle password visibility"
                    edge="end"
                  >
                    {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
            error={!!errors.confirmPassword}
            helperText={
              errors.confirmPassword && errors.confirmPassword.message
            }
            size="small"
          />
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            reset();
            handleCloseEdit();
          }}
          variant="contained"
          color="warning"
          sx={{ borderRadius: 20 }}
        >
          Hủy
        </Button>
        <Button
          type="submit"
          variant="contained"
          color="success"
          sx={{ borderRadius: 20 }}
        >
          Đổi mật khẩu
        </Button>
      </DialogActions>
    </form>
  );
};

export default EditPassWord;
