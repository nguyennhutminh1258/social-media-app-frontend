import { useTheme } from "@emotion/react";
import { Menu, MenuItem, Stack, Switch, Typography, useMediaQuery } from "@mui/material";
import { useContext } from "react";
import { ColorModeContext } from "../../utils/colorContext";
import LogoutIcon from "@mui/icons-material/Logout";
import NightlightRoundIcon from "@mui/icons-material/NightlightRound";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";

const MenuMore = ({ anchorEl, menuMoreId, isMenuOpen, handleMenuClose, handleLogout }) => {
  const navigate = useNavigate();
  const theme = useTheme();
  const colorMode = useContext(ColorModeContext);
  const userInfo = useSelector((state) => state.user.userInfo);
  const minWidth750 = useMediaQuery("(min-width:750px)");

  return (
    <Menu
      dense={"true"}
      anchorEl={anchorEl}
      id={menuMoreId}
      open={isMenuOpen}
      onClose={handleMenuClose}
      anchorOrigin={{
        vertical: "top",
        horizontal: "center",
      }}
      transformOrigin={{
        vertical: "bottom",
        horizontal: "center",
      }}>
      {!minWidth750 && (
        <MenuItem onClick={() => navigate(`/profile/${userInfo._id}`)}>
          <Stack width="100%" direction="row" alignItems="center" spacing={1}>
            <AccountCircleIcon size="small" />
            <Typography component="span" fontSize={15} fontWeight={400}>
              Thông tin cá nhân
            </Typography>
          </Stack>
        </MenuItem>
      )}
      <MenuItem disableRipple>
        <Stack width="100%" direction="row" justifyContent="space-between" alignItems="center">
          <Stack width="100%" direction="row" alignItems="center" spacing={1}>
            <NightlightRoundIcon size="small" />
            <Typography component="span" fontSize={15} fontWeight={400}>
              Chế độ tối:
            </Typography>
          </Stack>
          <Switch
            size="small"
            checked={theme.palette.mode === "dark"}
            onChange={colorMode.toggleColorMode}
            inputProps={{ "aria-label": "controlled" }}
          />
        </Stack>
      </MenuItem>
      <MenuItem onClick={handleLogout}>
        <Stack width="100%" direction="row" alignItems="center" spacing={1}>
          <LogoutIcon size="small" />
          <Typography component="span" fontSize={15} fontWeight={400}>
            Đăng xuất
          </Typography>
        </Stack>
      </MenuItem>
    </Menu>
  );
};

export default MenuMore;
