import CloseIcon from "@mui/icons-material/Close";
import {
  Avatar,
  Dialog,
  DialogTitle,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import { Stack } from "@mui/system";
import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import FollowBtn from "../follow-btn/FollowBtn";

const Following = ({ handleCloseFollowing, openFollowing, userProfile }) => {
  const navigate = useNavigate();
  const userInfo = useSelector((state) => state.user.userInfo);

  return (
    <Dialog onClose={handleCloseFollowing} open={openFollowing} fullWidth={true} maxWidth={"sm"}>
      <DialogTitle>
        <Typography sx={{ textAlign: "center", fontSize: "20px", fontWeight: 500, lineHeight: 1.5 }}>
          Người đang theo dõi
        </Typography>
        <IconButton
          aria-label="close"
          onClick={handleCloseFollowing}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      {userProfile?.following.length === 0 ? (
        <List>
          <Stack direction="column" justifyContent="center" alignItems="center">
            <ListItemText primary={"Bạn chưa đang theo dõi ai"} />
          </Stack>
        </List>
      ) : (
        <List sx={{ py: 0, width: "100%" }}>
          {userProfile?.following?.map((followingUser) => (
            <ListItem key={followingUser._id} divider={true}>
              <Stack
                direction="row"
                justifyContent="space-between"
                alignItems="center"
                spacing={{ md: 8, sm: 5, xs: 0 }}
                sx={{ width: "100%" }}>
                <Stack direction="row" alignItems="center" sx={{ width: "100%" }}>
                  <ListItemAvatar>
                    <Avatar src={followingUser.avatar} />
                  </ListItemAvatar>
                  <ListItemText
                    onClick={() => {
                      navigate(`/profile/${followingUser._id}`);
                      handleCloseFollowing();
                    }}
                    primary={
                      <Typography
                        sx={{
                          textOverflow: "ellipsis",
                          overflow: "hidden",
                          WebkitLineClamp: "1",
                          WebkitBoxOrient: "vertical",
                          whiteSpace: "nowrap",
                          fontSize: 18,
                          fontWeight: 600,
                          cursor: "pointer",
                          "@media (max-width: 450px)": {
                            maxWidth: "80px",
                          },
                        }}>
                        {followingUser.userName}
                      </Typography>
                    }
                    secondary={
                      <Typography
                        sx={{
                          textOverflow: "ellipsis",
                          overflow: "hidden",
                          WebkitLineClamp: "1",
                          WebkitBoxOrient: "vertical",
                          whiteSpace: "nowrap",
                          fontSize: 15,
                          fontWeight: 400,
                          "@media (max-width: 450px)": {
                            maxWidth: "80px",
                          },
                        }}>
                        {followingUser.name}
                      </Typography>
                    }
                  />
                </Stack>
                {userInfo._id !== followingUser._id && <FollowBtn userProfile={followingUser} />}
              </Stack>
            </ListItem>
          ))}
        </List>
      )}
    </Dialog>
  );
};

export default Following;
