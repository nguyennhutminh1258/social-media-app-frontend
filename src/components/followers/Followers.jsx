import CloseIcon from "@mui/icons-material/Close";
import {
  Avatar,
  Dialog,
  DialogTitle,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import { Stack } from "@mui/system";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import FollowBtn from "../follow-btn/FollowBtn";

const Followers = ({ handleCloseFollowers, openFollowers, userProfile }) => {
  const navigate = useNavigate();
  const userInfo = useSelector((state) => state.user.userInfo);

  return (
    <Dialog open={openFollowers} onClose={handleCloseFollowers} fullWidth={true} maxWidth={"sm"}>
      <DialogTitle>
        <Typography sx={{ textAlign: "center", fontSize: "20px", fontWeight: 500, lineHeight: 1.5 }}>
          Người theo dõi
        </Typography>
        <IconButton
          aria-label="close"
          onClick={handleCloseFollowers}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: (theme) => theme.palette.grey[500],
          }}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      {userProfile.followers?.length === 0 ? (
        <List>
          <Stack direction="column" justifyContent="center" alignItems="center">
            <ListItemText primary={"Bạn chưa có ai theo dõi"} />
          </Stack>
        </List>
      ) : (
        <List sx={{ py: 0, width: "100%" }}>
          {userProfile?.followers?.map((follower) => (
            <ListItem key={follower._id} divider={true}>
              <Stack
                direction="row"
                justifyContent="space-between"
                alignItems="center"
                spacing={{ md: 8, sm: 5, xs: 0 }}
                sx={{ width: "100%" }}>
                <Stack direction="row" alignItems="center" sx={{ width: "100%" }}>
                  <ListItemAvatar>
                    <Avatar src={follower.avatar} />
                  </ListItemAvatar>
                  <ListItemText
                    onClick={() => {
                      navigate(`/profile/${follower._id}`);
                      handleCloseFollowers();
                    }}
                    primary={
                      <Typography
                        sx={{
                          textOverflow: "ellipsis",
                          overflow: "hidden",
                          WebkitLineClamp: "1",
                          WebkitBoxOrient: "vertical",
                          whiteSpace: "nowrap",
                          fontSize: 18,
                          fontWeight: 600,
                          cursor: "pointer",
                          "@media (max-width: 450px)": {
                            maxWidth: "80px",
                          },
                        }}>
                        {follower.userName}
                      </Typography>
                    }
                    secondary={
                      <Typography
                        sx={{
                          textOverflow: "ellipsis",
                          overflow: "hidden",
                          WebkitLineClamp: "1",
                          WebkitBoxOrient: "vertical",
                          whiteSpace: "nowrap",
                          fontSize: 15,
                          fontWeight: 400,
                          "@media (max-width: 450px)": {
                            maxWidth: "80px",
                          },
                        }}>
                        {follower.name}
                      </Typography>
                    }
                  />
                </Stack>
                {userInfo._id !== follower._id && <FollowBtn userProfile={follower} />}
              </Stack>
            </ListItem>
          ))}
        </List>
      )}
    </Dialog>
  );
};

export default Followers;
