import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import { IconButton, useTheme } from "@mui/material";
import { Box } from "@mui/system";
import { useEffect, useState } from "react";
import LazyImage from "../lazy-image/LazyImage";

const ImageCarosel = ({ images, detail }) => {
  const theme = useTheme();

  const [index, setIndex] = useState(0);
  const [showPrevBtn, setShowPrevBtn] = useState(false);
  const [showNextBtn, setShowNextBtn] = useState(false);

  const handlePrevImage = () => {
    setIndex((index) => {
      return index > 0 ? index - 1 : images?.length - 1;
    });
  };

  const handleNextImage = () => {
    setIndex((index) => {
      return index < images?.length - 1 ? index + 1 : 0;
    });
  };

  useEffect(() => {
    setShowPrevBtn(index > 0);
    setShowNextBtn(index < images?.length - 1);
  }, [images?.length, index]);

  return (
    <Box sx={{ width: "100%", position: "relative" }}>
      <Box width="100%" sx={{ position: "relative" }}>
        {images?.length > 1 && showPrevBtn && (
          <IconButton
            sx={{
              position: "absolute",
              top: "50%",
              left: 10,
              zIndex: 10,
              color: theme.palette.text.primary,
            }}
            onClick={handlePrevImage}
          >
            <ArrowBackIosNewIcon size="large" />
          </IconButton>
        )}

        <Box
          sx={{ display: "flex", justifyContent: "center", width: "100%" }}
        >
          {images?.map((image, i) => {
            return (
              <LazyImage
                detail={detail}
                key={image?.url}
                src={image?.url}
                index={index}
                i={i}
              />
            );
          })}
        </Box>

        {images?.length > 1 && showNextBtn && (
          <IconButton
            sx={{
              position: "absolute",
              top: "50%",
              right: 10,
              zIndex: 10,
              color: theme.palette.text.primary,
            }}
            onClick={handleNextImage}
          >
            <ArrowForwardIosIcon size="large" />
          </IconButton>
        )}
      </Box>
    </Box>
  );
};

export default ImageCarosel;
