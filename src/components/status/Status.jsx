import { Avatar, Button, Paper } from "@mui/material";
import { alpha, styled } from "@mui/material/styles";
import { Box, Stack } from "@mui/system";
import { useDispatch, useSelector } from "react-redux";
import { setShowStatus } from "../../store/slice/statusSlice";

const ButtonStatus = styled(Button)(({ theme }) => ({
  color: theme.palette.text.primary,
  backgroundColor: alpha(theme.palette.common.black, 0.1),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.black, 0.15),
  },
}));

const Status = () => {
  const dispatch = useDispatch();
  const userInfo = useSelector((state) => state.user.userInfo);

  return (
    <Paper elevation={2}>
      <Stack direction="row" p={2} alignItems="center" spacing={2}>
        <Box>
          <Avatar
            src={userInfo.avatar}
            sx={{
              maxHeight: { xs: 35, md: 40, lg: 45 },
              maxWidth: { xs: 35, md: 40, lg: 45 },
              borderRadius: "50%",
              objectFit: "cover",
            }}
            alt="avatar-user"
          />
        </Box>
        <Box sx={{ width: "100%", borderRadius: 20 }}>
          <ButtonStatus
            variant="text"
            sx={{
              textTransform: "none",
              width: "100%",
              paddingLeft: 3,
              borderRadius: 20,
              justifyContent: "flex-start",
            }}
            onClick={() => dispatch(setShowStatus(true))}>
            Bạn đang nghĩ gì thế?
          </ButtonStatus>
        </Box>
      </Stack>
    </Paper>
  );
};

export default Status;
