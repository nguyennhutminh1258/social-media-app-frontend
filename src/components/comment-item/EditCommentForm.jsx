import { IconButton, InputBase, styled } from "@mui/material";
import { Stack } from "@mui/system";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { updateComment } from "../../store/slice/commentSlice";
import SendOutlinedIcon from "@mui/icons-material/SendOutlined";
import SentimentVerySatisfiedIcon from "@mui/icons-material/SentimentVerySatisfied";
import { useEffect, useState } from "react";
import EmojiMenu from "../emoji-menu/EmojiMenu";

const Input = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  width: "100%",
  background: theme.palette.mode === "dark" ? "#181818" : "#eee",
  padding: 5,
  borderRadius: 20,
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 1.5),
    width: "100%",
    fontSize: "13px",
  },
}));

const EditCommentForm = ({ post, comment, content, setIsEditComment }) => {
  const dispatch = useDispatch();
  //Thông tin user và token hiện đang login
  const userInfo = useSelector((state) => state.user.userInfo);
  //Form của phần edit comment
  const { register, handleSubmit, setValue, getValues } = useForm();
  // Hiện menu emoji
  const [anchorEmoji, setAnchorEmoji] = useState(null);
  const openEmoji = Boolean(anchorEmoji);

  const handleOpenEmoji = (event) => {
    setAnchorEmoji(event.currentTarget);
  };
  const handleCloseEmoji = () => {
    setAnchorEmoji(null);
  };

  const handleUpdateComment = (data) => {
    if (data.content === content) {
      setIsEditComment(false);
    } else {
      dispatch(updateComment(data));
      setIsEditComment(false);
    }
  };

  useEffect(() => {
    setValue("post", post);
    setValue("comment", comment);
    setValue("user", userInfo);
  }, [setValue, comment, post, userInfo]);

  return (
    <>
      <form onSubmit={handleSubmit(handleUpdateComment)}>
        <Stack direction="row" alignItems="center">
          <Input
            {...register("content", {
              required: true,
            })}
            multiline={true}
            defaultValue={content}
            size="small"
            fullWidth
          />
          <IconButton onClick={handleOpenEmoji}>
            <SentimentVerySatisfiedIcon />
          </IconButton>
          <IconButton type="submit">
            <SendOutlinedIcon />
          </IconButton>
        </Stack>
      </form>
      <EmojiMenu
        anchorEmoji={anchorEmoji}
        openEmoji={openEmoji}
        handleCloseEmoji={handleCloseEmoji}
        setValue={setValue}
        getValues={getValues}
      />
    </>
  );
};

export default EditCommentForm;
