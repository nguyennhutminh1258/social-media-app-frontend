import { Avatar, IconButton, InputBase, Stack, styled, Typography, useMediaQuery } from "@mui/material";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { createComment } from "../../store/slice/commentSlice";
import EmojiMenu from "../emoji-menu/EmojiMenu";
import { Link, useNavigate } from "react-router-dom";
import SendOutlinedIcon from "@mui/icons-material/SendOutlined";
import SentimentVerySatisfiedIcon from "@mui/icons-material/SentimentVerySatisfied";

const Input = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  width: "100%",
  background: theme.palette.mode === "dark" ? "#181818" : "#eee",
  padding: 5,
  borderRadius: 20,
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 1.5),
    width: "100%",
    fontSize: "13px",
  },
}));

const ReplyCommentForm = ({ user, post, commentId, setReply }) => {
  const minWidth750 = useMediaQuery("(min-width:750px)");
  const navigate = useNavigate();
  const dispatch = useDispatch();
  //Thông tin user và token hiện đang login
  const userInfo = useSelector((state) => state.user.userInfo);
  const token = useSelector((state) => state.user.token);
  //Socket
  const socket = useSelector((state) => state.socket);
  //Form của phần reply comment
  const { register: registerReply, handleSubmit, setValue, getValues } = useForm();
  // Hiện menu emoji
  const [anchorEmoji, setAnchorEmoji] = useState(null);
  const openEmoji = Boolean(anchorEmoji);

  const handleOpenEmoji = (event) => {
    setAnchorEmoji(event.currentTarget);
  };
  const handleCloseEmoji = () => {
    setAnchorEmoji(null);
  };

  const handleReply = (data) => {
    const newComment = {
      content: data.content,
      likes: [],
      user: userInfo,
      createdAt: new Date().toISOString(),
      reply: commentId,
      tag: user,
    };

    if (token) {
      dispatch(createComment({ post, newComment, socket }));
      setReply(false);
    } else {
      navigate("/login");
    }
  };

  return (
    <Stack width="100%" direction="row" alignItems="center" spacing={2} marginBottom={1}>
      <Avatar src={userInfo.avatar} sx={{ width: 30, height: 30 }} />
      <form style={{ width: "100%" }} onSubmit={handleSubmit(handleReply)}>
        <Stack width="100%" direction="row" alignItems="center">
          <Input
            {...registerReply("content", {
              required: true,
            })}
            multiline={true}
            size="small"
            autoFocus
            fullWidth
            sx={{ paddingLeft: 2 }}
            startAdornment={
              <Link to={`/profile/${user?._id}`} style={{ textDecoration: "none", color: "inherit" }}>
                <Typography
                  component="span"
                  fontSize={minWidth750 ? 14 : 12}
                  fontWeight={600}
                  sx={{ backgroundColor: "rgba(24, 119, 242, 0.45)" }}>
                  {user.userName}
                </Typography>
              </Link>
            }
          />
          <IconButton onClick={handleOpenEmoji}>
            <SentimentVerySatisfiedIcon />
          </IconButton>
          <IconButton type="submit">
            <SendOutlinedIcon />
          </IconButton>
        </Stack>
      </form>
      <EmojiMenu
        anchorEmoji={anchorEmoji}
        openEmoji={openEmoji}
        handleCloseEmoji={handleCloseEmoji}
        setValue={setValue}
        getValues={getValues}
      />
    </Stack>
  );
};

export default ReplyCommentForm;
