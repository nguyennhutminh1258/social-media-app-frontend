import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import { Avatar, Box, Button, IconButton, Menu, MenuItem, Typography, useMediaQuery } from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { Stack } from "@mui/system";
import moment from "moment";
import { memo, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { deleteComment, likeComment, unLikeComment } from "../../store/slice/commentSlice";
import LikeButton from "../like-button/LikeButton";
import ReplyCommentForm from "./ReplyCommentForm";
import EditCommentForm from "./EditCommentForm";

const MenuComment = ({ handleShowEditComment, handleRemoveComment }) => {
  return (
    <Box>
      <MenuItem onClick={handleShowEditComment} disableRipple>
        <EditIcon sx={{ marginRight: 1.5 }} fontSize="small" />
        <Typography component="span" fontSize={15}>
          Sửa bình luận
        </Typography>
      </MenuItem>
      <MenuItem onClick={handleRemoveComment} disableRipple>
        <DeleteIcon sx={{ marginRight: 1.5 }} fontSize="small" />
        <Typography component="span" fontSize={15}>
          Xóa bình luận
        </Typography>
      </MenuItem>
    </Box>
  );
};

const CommentItem = ({ comment, post, replyComment, commentId }) => {
  const minWidth750 = useMediaQuery("(min-width:750px)");
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const theme = useTheme();
  //User post commment và nội dung comment
  const { user, content } = comment;
  //Thông tin user và token hiện đang login
  const userInfo = useSelector((state) => state.user.userInfo);
  const token = useSelector((state) => state.user.token);
  //Socket
  const socket = useSelector((state) => state.socket);
  //Menu
  const [anchorElMenu, setAnchorElMenu] = useState(null);
  const open = Boolean(anchorElMenu);
  //Read More
  const [readMore, setReadMore] = useState(false);
  //Like Comment
  const [likedComment, setLikedComment] = useState(false);
  //Edit Comment
  const [isEditComment, setIsEditComment] = useState(false);
  //Reply Comment
  const [reply, setReply] = useState(false);
  //Show more Reply Comment
  const [showReply, setShowReply] = useState([]);
  const [next, setNext] = useState(1);

  const handleOpenMenu = (event) => {
    setAnchorElMenu(event.currentTarget);
  };
  const handleCloseMenu = () => {
    setAnchorElMenu(null);
  };

  const handleReadMore = () => {
    setReadMore(!readMore);
  };

  const handleLikeComment = () => {
    // Kiểm tra user đã đăng nhập chưa
    if (token) {
      setLikedComment(true);
      dispatch(likeComment({ comment, post, userInfo }));
    } else {
      navigate("/login");
    }
  };

  const handleUnLikeComment = () => {
    setLikedComment(false);
    dispatch(unLikeComment({ comment, post, userInfo }));
  };

  const handleShowEditComment = () => {
    setIsEditComment(true);
    handleCloseMenu();
  };

  const handleCancelEditComment = () => {
    setIsEditComment(false);
  };

  const handleRemoveComment = () => {
    dispatch(deleteComment({ comment, post, userInfo, socket }));
    handleCloseMenu();
  };

  useEffect(() => {
    setLikedComment(false);
    setReply(false);
    if (comment.likes?.find((like) => like._id === userInfo._id)) {
      setLikedComment(true);
    }
  }, [comment.likes, userInfo._id]);

  useEffect(() => {
    setShowReply(replyComment?.slice(0, next));
  }, [next, replyComment]);

  return (
    <>
      <Stack
        direction="row"
        spacing={2}
        alignItems="flex-start"
        sx={{
          opacity: comment._id ? 1 : 0.5,
          pointerEvents: comment._id ? "inherit" : "none",
          marginBottom: 1,
        }}>
        <Avatar
          src={user?.avatar}
          sx={{ width: `${minWidth750 ? "40px" : "30px"}`, height: `${minWidth750 ? "40px" : "30px"}` }}
        />
        <Stack width="100%">
          <Stack
            direction="row"
            justifyContent="space-between"
            alignItems="flex-start"
            sx={{ width: "100%" }}>
            <Stack width={isEditComment ? "100%" : "fit-content"}>
              {isEditComment ? (
                <EditCommentForm
                  post={post}
                  comment={comment}
                  content={content}
                  setIsEditComment={setIsEditComment}
                />
              ) : (
                <Stack
                  sx={{
                    wordBreak: "break-all",
                    background: theme.palette.mode === "dark" ? "#181818" : "#eee",
                    padding: "10px 15px",
                    borderRadius: 3,
                  }}>
                  <Link to={`/profile/${user?._id}`} style={{ textDecoration: "none", color: "inherit" }}>
                    <Typography
                      component="span"
                      fontSize={minWidth750 ? 14 : 12}
                      fontWeight={600}
                      sx={{ marginRight: 1 }}>
                      {user?.userName}:
                    </Typography>
                  </Link>
                  <Typography component="span">
                    {comment.tag && comment.tag._id !== comment.user._id && (
                      <Link
                        to={`/profile/${comment.tag._id}`}
                        style={{ textDecoration: "none", color: "rgba(24, 119, 242, 0.45)" }}>
                        <Typography component="span" fontSize={minWidth750 ? 14 : 12} fontWeight={600}>
                          {comment.tag.userName}{" "}
                        </Typography>
                      </Link>
                    )}
                    <Typography
                      component="span"
                      fontSize={minWidth750 ? 13 : 11}
                      sx={{ whiteSpace: "pre-line" }}>
                      {content?.length < 100
                        ? content
                        : readMore
                        ? content + " "
                        : content?.slice(0, 100) + "..."}
                    </Typography>{" "}
                    {content?.length > 100 && (
                      <Typography
                        component="span"
                        fontSize={minWidth750 ? 14 : 12}
                        fontWeight={700}
                        sx={{
                          cursor: "pointer",
                          "&:hover": {
                            textDecoration: "underline",
                          },
                        }}
                        onClick={handleReadMore}>
                        {readMore ? "Ẩn bớt" : "Xem thêm"}
                      </Typography>
                    )}
                  </Typography>
                </Stack>
              )}
              {isEditComment ? (
                <Stack direction="row" spacing={1} alignItems="center">
                  <Typography component="span" fontSize={minWidth750 ? 13 : 11} fontWeight={300}>
                    {moment(comment?.createdAt).fromNow()}
                  </Typography>
                  <Button
                    size="small"
                    sx={{
                      color: theme.palette.text.primary,
                      textTransform: "none",
                      "&:hover": {
                        backgroundColor: "transparent",
                        textDecoration: "underline",
                      },
                    }}
                    onClick={handleCancelEditComment}
                    disableRipple>
                    Hủy
                  </Button>
                </Stack>
              ) : (
                <Stack direction="row" spacing={1} alignItems="center">
                  <Typography component="span" fontSize={minWidth750 ? 13 : 11} fontWeight={300}>
                    {moment(comment?.createdAt).fromNow()}
                  </Typography>

                  <Typography component="span" fontSize={minWidth750 ? 13 : 11} fontWeight={300}>
                    {comment.likes?.length} lượt thích
                  </Typography>

                  {token && (
                    <Button
                      size="small"
                      sx={{
                        color: theme.palette.text.primary,
                        fontSize: `${minWidth750 ? "13px" : "11px"}`,
                        textTransform: "none",
                        "&:hover": {
                          backgroundColor: "transparent",
                          textDecoration: "underline",
                        },
                      }}
                      onClick={() => {
                        setReply(!reply);
                      }}>
                      {reply ? "Hủy" : "Phản hồi"}
                    </Button>
                  )}

                  {isEditComment ? (
                    <></>
                  ) : (
                    <Box>
                      {(user?._id === userInfo?._id || post.user?._id === userInfo?._id) && (
                        <IconButton onClick={handleOpenMenu}>
                          <MoreHorizIcon />
                        </IconButton>
                      )}
                    </Box>
                  )}
                </Stack>
              )}
            </Stack>
            <LikeButton
              isComment={true}
              liked={likedComment}
              handleLike={handleLikeComment}
              handleUnlike={handleUnLikeComment}
            />
          </Stack>
          {reply && <ReplyCommentForm user={user} post={post} commentId={commentId} setReply={setReply} />}
          {showReply?.map(
            (item) =>
              item.reply && (
                <CommentItem
                  key={item._id ? item._id : item.createdAt}
                  comment={item}
                  post={post}
                  commentId={comment._id}
                />
              )
          )}
          {replyComment?.length - next > 0 ? (
            <Stack>
              <Typography
                component="span"
                fontSize={minWidth750 ? 14 : 12}
                sx={{
                  cursor: "pointer",
                  fontWeight: 600,
                  "&:hover": {
                    textDecoration: "underline",
                  },
                }}
                onClick={() => {
                  setNext(next + 10);
                  setReply(false);
                }}>
                Xem thêm bình luận
              </Typography>
            </Stack>
          ) : (
            replyComment?.length > 1 &&
            next >= replyComment?.length && (
              <Stack>
                <Typography
                  component="span"
                  sx={{
                    cursor: "pointer",
                    fontSize: `${minWidth750} ? "14" : "12"`,
                    fontWeight: 600,
                    "&:hover": {
                      textDecoration: "underline",
                    },
                  }}
                  onClick={() => {
                    setNext(1);
                    setReply(false);
                  }}>
                  Ẩn bớt bình luận
                </Typography>
              </Stack>
            )
          )}
        </Stack>
      </Stack>
      <Menu
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        anchorEl={anchorElMenu}
        open={open}
        onClose={handleCloseMenu}>
        {post.user?._id === userInfo?._id ? (
          user?._id === userInfo?._id ? (
            <MenuComment
              handleShowEditComment={handleShowEditComment}
              handleRemoveComment={handleRemoveComment}
            />
          ) : (
            <Box>
              <MenuItem onClick={handleRemoveComment} disableRipple>
                <DeleteIcon sx={{ marginRight: 1.5 }} fontSize="small" />
                <Typography component="span" fontSize={15}>
                  Xóa bình luận
                </Typography>
              </MenuItem>
            </Box>
          )
        ) : (
          user._id === userInfo._id && (
            <MenuComment
              handleShowEditComment={handleShowEditComment}
              handleRemoveComment={handleRemoveComment}
            />
          )
        )}
      </Menu>
    </>
  );
};

export default memo(CommentItem);
