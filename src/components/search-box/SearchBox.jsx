import SearchIcon from "@mui/icons-material/Search";
import { Avatar, List, ListItem, ListItemAvatar, ListItemText, Typography } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { searchUser } from "../../store/slice/searchSlice";
import InputSearch from "./InputSearch";
import Search from "./Search";
import SearchIconWrapper from "./SearchIconWrapper";

const SearchBox = ({ search, setSearch }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  // Danh sách user tìm kiếm
  const searchList = useSelector((state) => state.search);

  const handleOnSearch = (e) => {
    setSearch(e.target.value.toLowerCase().replace(/ /g, ""));
  };

  useEffect(() => {
    // limit request to server
    const getData = setTimeout(() => {
      dispatch(searchUser(search));
    }, 500);

    return () => clearTimeout(getData);
  }, [dispatch, search]);

  return (
    <>
      <Search>
        <SearchIconWrapper>
          <SearchIcon />
        </SearchIconWrapper>
        <InputSearch
          placeholder="Tìm kiếm..."
          inputProps={{ "aria-label": "search" }}
          value={search}
          onChange={(e) => handleOnSearch(e)}
        />
        {searchList.length > 0 && (
          <List
            sx={{
              borderRadius: 2,
              width: "100%",
              maxWidth: "inherit",
              maxHeight: 600,
              overflowY: "auto",
              bgcolor: "background.paper",
              position: "fixed",
              top: 60,
              zIndex: 1000,
            }}>
            {searchList?.map((item) => {
              return (
                <ListItem alignItems="flex-start" key={item._id} divider={true}>
                  <ListItemAvatar>
                    <Avatar alt="User Avatar" src={item.avatar} />
                  </ListItemAvatar>
                  <ListItemText
                    primary={
                      <>
                        <Typography
                          sx={{ display: "inline", cursor: "pointer" }}
                          component="span"
                          variant="body2"
                          color="text.primary"
                          onClick={() => {
                            navigate(`/profile/${item._id}`);
                            setSearch("");
                          }}>
                          {item.name}
                        </Typography>
                      </>
                    }
                    secondary={
                      <>
                        <Typography
                          sx={{ display: "inline" }}
                          component="span"
                          variant="body2"
                          color="text.secondary">
                          {item.userName}
                        </Typography>
                      </>
                    }
                  />
                </ListItem>
              );
            })}
          </List>
        )}
      </Search>
    </>
  );
};

export default SearchBox;
