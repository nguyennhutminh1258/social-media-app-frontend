import FavoriteIcon from "@mui/icons-material/Favorite";
import FavoriteBorderOutlinedIcon from "@mui/icons-material/FavoriteBorderOutlined";
import { IconButton } from "@mui/material";

const LikeButton = ({ liked, handleLike, handleUnlike, isComment }) => {
  return (
    <>
      {liked ? (
        <IconButton
          aria-label="add to favorites"
          size={`${isComment ? "small" : "large"}`}
          onClick={handleUnlike}>
          <FavoriteIcon sx={{ color: "#fa383e", width: isComment ? 18 : 30, height: isComment ? 18 : 30 }} />
        </IconButton>
      ) : (
        <IconButton
          aria-label="add to favorites"
          size={`${isComment ? "small" : "large"}`}
          onClick={handleLike}>
          <FavoriteBorderOutlinedIcon sx={{ width: isComment ? 18 : 30, height: isComment ? 18 : 30 }} />
        </IconButton>
      )}
    </>
  );
};

export default LikeButton;
