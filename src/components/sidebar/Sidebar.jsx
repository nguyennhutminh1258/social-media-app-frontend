import NotificationsIcon from "@mui/icons-material/Notifications";
import { Avatar, Badge, Box, Paper, Stack, Typography, useMediaQuery } from "@mui/material";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { logout } from "../../store/slice/userSlice";
import MenuMore from "../menu-more/MenuMore";
import PeopleIcon from "@mui/icons-material/People";
import DehazeIcon from "@mui/icons-material/Dehaze";
import HomeIcon from "@mui/icons-material/Home";
import MobileMode750 from "./MobileMode750";
import MobileMode1175 from "./MobileMode1175";
import { useTheme } from "@emotion/react";
import TelegramIcon from "@mui/icons-material/Telegram";

const Sidebar = () => {
  const { pathname } = useLocation();
  const theme = useTheme();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const minWidth1175 = useMediaQuery("(min-width:1175px)");
  const minWidth750 = useMediaQuery("(min-width:750px)");

  const token = useSelector((state) => state.user.token);
  const userInfo = useSelector((state) => state.user.userInfo);
  const notifies = useSelector((state) => state.notify);
  const userSuggestion = useSelector((state) => state.suggestion.userSuggestion);

  // Hiện menu xem thêm
  const [anchorEl, setAnchorEl] = useState(null);
  const menuMoreId = "primary-more-menu";
  const isMenuOpen = Boolean(anchorEl);

  const handleMenuMoreOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    dispatch(logout());
    window.location.reload();
  };

  return (
    <Paper
      sx={{
        width: minWidth1175 ? "20vw" : minWidth750 ? "10vw" : "100vw",
        position: minWidth750 ? "sticky" : "fixed",
        top: minWidth750 ? 65 : "",
        bottom: minWidth750 ? "" : 0,
        zIndex: 1000,
        height: minWidth750 ? "calc(100vh - 65px)" : "60px",
        padding: minWidth1175 ? 3 : 0,
        paddingTop: 3,
        display: "flex",
        justifyContent: minWidth750 ? "" : "center",
        alignItems: minWidth750 ? "" : "center",
        backgroundColor: theme.palette.mode === "dark" ? "#121212" : "#f9f9f9",
      }}>
      {minWidth750 ? (
        <Stack
          direction="column"
          spacing={1}
          alignItems="center"
          justifyContent="space-between"
          sx={{ height: "100%", width: "100%" }}>
          <Box sx={{ width: "100%" }}>
            {token && (
              <Stack direction="column" alignItems={minWidth1175 ? "" : "center"} spacing={3}>
                {minWidth1175 ? (
                  <>
                    <Stack
                      sx={{
                        height: 50,
                        paddingLeft: 3,
                        borderRadius: 4,
                        backgroundColor:
                          pathname === "/"
                            ? theme.palette.mode === "dark"
                              ? "#666"
                              : "rgba(0, 0, 0, 0.1)"
                            : "transparent",
                        "&:hover": {
                          backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
                          borderRadius: 4,
                        },
                        cursor: "pointer",
                      }}
                      direction="row"
                      alignItems="center"
                      onClick={() => navigate("/")}>
                      <Box sx={{ marginRight: 2, width: 50 }}>
                        <HomeIcon />
                      </Box>
                      <Typography component="span" fontSize={15} fontWeight={pathname === "/" ? 600 : 400}>
                        Trang Chủ
                      </Typography>
                    </Stack>
                    <Stack
                      sx={{
                        height: 50,
                        paddingLeft: 3,
                        borderRadius: 4,
                        backgroundColor:
                          pathname === "/suggestion"
                            ? theme.palette.mode === "dark"
                              ? "#666"
                              : "rgba(0, 0, 0, 0.1)"
                            : "transparent",
                        "&:hover": {
                          backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
                          borderRadius: 4,
                        },
                        cursor: "pointer",
                      }}
                      direction="row"
                      alignItems="center"
                      onClick={() => navigate("/suggestion")}>
                      <Box sx={{ marginRight: 2, width: 50 }}>
                        <Badge badgeContent={userSuggestion?.length} color="error">
                          <PeopleIcon />
                        </Badge>
                      </Box>
                      <Typography
                        component="span"
                        fontSize={15}
                        fontWeight={pathname === "/suggestion" ? 600 : 400}>
                        Đề xuất theo dõi
                      </Typography>
                    </Stack>
                    <Stack
                      sx={{
                        height: 50,
                        paddingLeft: 3,
                        borderRadius: 4,
                        backgroundColor:
                          pathname === "/notification"
                            ? theme.palette.mode === "dark"
                              ? "#666"
                              : "rgba(0, 0, 0, 0.1)"
                            : "transparent",
                        "&:hover": {
                          backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
                          borderRadius: 4,
                        },
                        cursor: "pointer",
                      }}
                      direction="row"
                      alignItems="center"
                      onClick={() => navigate("/notification")}>
                      <Box sx={{ marginRight: 2, width: 50 }}>
                        <Badge
                          badgeContent={notifies?.filter((notify) => notify.isRead === false).length}
                          color="error">
                          <NotificationsIcon />
                        </Badge>
                      </Box>
                      <Typography
                        component="span"
                        fontSize={15}
                        fontWeight={pathname === "/notification" ? 600 : 400}>
                        Thông báo
                      </Typography>
                    </Stack>

                    <Stack
                      sx={{
                        height: 50,
                        paddingLeft: 3,
                        borderRadius: 4,
                        backgroundColor:
                          pathname === "/message"
                            ? theme.palette.mode === "dark"
                              ? "#666"
                              : "rgba(0, 0, 0, 0.1)"
                            : "transparent",
                        "&:hover": {
                          backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
                          borderRadius: 4,
                        },
                        cursor: "pointer",
                      }}
                      direction="row"
                      alignItems="center"
                      onClick={() => navigate("/message")}>
                      <Box sx={{ marginRight: 2, width: 50 }}>
                        {/* <Badge
                          badgeContent={notifies?.filter((notify) => notify.isRead === false).length}
                          color="error"> */}
                        <TelegramIcon />
                        {/* </Badge> */}
                      </Box>
                      <Typography
                        component="span"
                        fontSize={15}
                        fontWeight={pathname === "/notification" ? 600 : 400}>
                        Tin nhắn
                      </Typography>
                    </Stack>
                    <Stack
                      sx={{
                        height: 50,
                        paddingLeft: 3,
                        borderRadius: 4,
                        backgroundColor:
                          pathname === `/profile/${userInfo._id}`
                            ? theme.palette.mode === "dark"
                              ? "#666"
                              : "rgba(0, 0, 0, 0.1)"
                            : "transparent",
                        "&:hover": {
                          backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
                          borderRadius: 4,
                        },
                        cursor: "pointer",
                      }}
                      direction="row"
                      alignItems="center"
                      onClick={() => navigate(`/profile/${userInfo._id}`)}
                      color="inherit">
                      <Box sx={{ marginRight: 2, width: 50 }}>
                        <Avatar
                          sx={{
                            maxHeight: { xs: 35, md: 40, lg: 45 },
                            maxWidth: { xs: 35, md: 40, lg: 45 },
                            borderRadius: "50%",
                            objectFit: "cover",
                          }}
                          alt="avatar-user"
                          src={userInfo.avatar}
                        />
                      </Box>
                      <Typography
                        component="span"
                        fontSize={15}
                        fontWeight={pathname === `/profile/${userInfo._id}` ? 600 : 400}>
                        Trang cá nhân
                      </Typography>
                    </Stack>
                  </>
                ) : (
                  <MobileMode1175 />
                )}
              </Stack>
            )}
          </Box>
          {minWidth1175 ? (
            <Stack
              sx={{
                width: "100%",
                height: 50,
                paddingLeft: 3,
                "&:hover": {
                  backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
                  borderRadius: 4,
                },
                cursor: "pointer",
              }}
              direction="row"
              alignItems="center"
              aria-label="account of current user"
              aria-controls={menuMoreId}
              aria-haspopup="true"
              onClick={handleMenuMoreOpen}>
              <DehazeIcon sx={{ marginRight: 2 }} />
              <Typography component="span" fontSize={15}>
                Xem thêm
              </Typography>
            </Stack>
          ) : (
            <Stack
              sx={{
                height: 50,
                paddingLeft: 3,
                "&:hover": {
                  backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
                  borderRadius: 4,
                },
                cursor: "pointer",
              }}
              direction="row"
              alignItems="center"
              justifyContent="center"
              aria-label="account of current user"
              aria-controls={menuMoreId}
              aria-haspopup="true"
              onClick={handleMenuMoreOpen}>
              <Box sx={{ width: 50 }}>
                <DehazeIcon />
              </Box>
            </Stack>
          )}
        </Stack>
      ) : (
        <MobileMode750 menuMoreId={menuMoreId} handleMenuMoreOpen={handleMenuMoreOpen} />
      )}

      <MenuMore
        anchorEl={anchorEl}
        menuMoreId={menuMoreId}
        isMenuOpen={isMenuOpen}
        handleMenuClose={handleMenuClose}
        handleLogout={handleLogout}
      />
    </Paper>
  );
};

export default Sidebar;
