import { Box, Stack } from "@mui/system";
import React from "react";
import { useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import HomeIcon from "@mui/icons-material/Home";
import PeopleIcon from "@mui/icons-material/People";
import NotificationsIcon from "@mui/icons-material/Notifications";
import { Avatar, Badge, useTheme } from "@mui/material";
import TelegramIcon from "@mui/icons-material/Telegram";

const MobileMode750 = ({ handleMenuMoreOpen }) => {
  const { pathname } = useLocation();
  const theme = useTheme();
  const navigate = useNavigate();

  const userInfo = useSelector((state) => state.user.userInfo);
  const notifies = useSelector((state) => state.notify);
  const userSuggestion = useSelector((state) => state.suggestion.userSuggestion);

  return (
    <Stack direction="row" sx={{ paddingTop: 0, paddingBottom: 3 }}>
      <Stack
        sx={{
          height: 50,
          paddingLeft: 3,
          borderRadius: 4,
          backgroundColor:
            pathname === "/"
              ? theme.palette.mode === "dark"
                ? "#666"
                : "rgba(0, 0, 0, 0.1)"
              : "transparent",
          "&:hover": {
            backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
            borderRadius: 4,
          },
          cursor: "pointer",
        }}
        direction="row"
        alignItems="center"
        justifyContent="center"
        onClick={() => navigate("/")}>
        <Box sx={{ width: 50 }}>
          <HomeIcon />
        </Box>
      </Stack>
      <Stack
        sx={{
          height: 50,
          paddingLeft: 3,
          borderRadius: 4,
          backgroundColor:
            pathname === "/suggestion"
              ? theme.palette.mode === "dark"
                ? "#666"
                : "rgba(0, 0, 0, 0.1)"
              : "transparent",
          "&:hover": {
            backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
            borderRadius: 4,
          },
          cursor: "pointer",
        }}
        direction="row"
        alignItems="center"
        justifyContent="center"
        onClick={() => navigate("/suggestion")}>
        <Box sx={{ width: 50 }}>
          <Badge badgeContent={userSuggestion?.length} color="error">
            <PeopleIcon />
          </Badge>
        </Box>
      </Stack>
      <Stack
        sx={{
          height: 50,
          paddingLeft: 3,
          borderRadius: 4,
          backgroundColor:
            pathname === "/notification"
              ? theme.palette.mode === "dark"
                ? "#666"
                : "rgba(0, 0, 0, 0.1)"
              : "transparent",
          "&:hover": {
            backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
            borderRadius: 4,
          },
          cursor: "pointer",
        }}
        direction="row"
        alignItems="center"
        justifyContent="center"
        onClick={() => navigate("/notification")}>
        <Box sx={{ width: 50 }}>
          <Badge badgeContent={notifies?.filter((notify) => notify.isRead === false).length} color="error">
            <NotificationsIcon />
          </Badge>
        </Box>
      </Stack>
      <Stack
        sx={{
          height: 50,
          paddingLeft: 3,
          borderRadius: 4,
          backgroundColor:
            pathname === "/notification"
              ? theme.palette.mode === "dark"
                ? "#666"
                : "rgba(0, 0, 0, 0.1)"
              : "transparent",
          "&:hover": {
            backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
            borderRadius: 4,
          },
          cursor: "pointer",
        }}
        direction="row"
        alignItems="center"
        justifyContent="center"
        onClick={() => navigate("/message")}>
        <Box sx={{ width: 50 }}>
          {/* <Badge badgeContent={notifies?.filter((notify) => notify.isRead === false).length} color="error"> */}
          <TelegramIcon />
          {/* </Badge> */}
        </Box>
      </Stack>
      <Stack
        sx={{
          height: 50,
          width: 75,
          paddingLeft: 1,
          borderRadius: 4,
          "&:hover": {
            backgroundColor: theme.palette.mode === "dark" ? "#666" : "rgba(0, 0, 0, 0.1)",
            borderRadius: 4,
          },
          cursor: "pointer",
        }}
        direction="row"
        alignItems="center"
        justifyContent="center"
        onClick={handleMenuMoreOpen}>
        <Box sx={{ width: 45, alignSelf: "center" }}>
          <Avatar
            sx={{
              borderRadius: "50%",
              objectFit: "cover",
            }}
            alt="avatar-user"
            src={userInfo.avatar}
          />
        </Box>
      </Stack>
    </Stack>
  );
};

export default MobileMode750;
