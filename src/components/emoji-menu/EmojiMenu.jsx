import { Menu, useTheme } from "@mui/material";
import EmojiPicker from "emoji-picker-react";

const EmojiMenu = ({ anchorEmoji, openEmoji, handleCloseEmoji, setValue, getValues }) => {
  const theme = useTheme();

  const handleEmojiClick = (emoji) => {
    let content = getValues("content");
    setValue("content", content + emoji.emoji);
  };

  return (
    <Menu
      id="basic-menu"
      anchorEl={anchorEmoji}
      open={openEmoji}
      onClose={handleCloseEmoji}
      anchorOrigin={{
        vertical: "top",
        horizontal: "center",
      }}
      transformOrigin={{
        vertical: "bottom",
        horizontal: "center",
      }}>
      <EmojiPicker
        theme={theme.palette.mode === "dark" ? "dark" : "light"}
        height={250}
        width={300}
        searchDisabled={true}
        lazyLoadEmojis={true}
        previewConfig={{ showPreview: false }}
        emojiStyle={"native"}
        onEmojiClick={handleEmojiClick}
      />
    </Menu>
  );
};

export default EmojiMenu;
