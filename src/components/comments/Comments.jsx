import { Typography, useMediaQuery } from "@mui/material";
import { Stack } from "@mui/system";
import React, { useEffect, useState } from "react";
import CommentItem from "../comment-item/CommentItem";

const Comments = ({ post }) => {
  const minWidth750 = useMediaQuery("(min-width:750px)");
  const [comments, setComments] = useState([]);
  const [showComment, setShowComment] = useState([]);
  const [next, setNext] = useState(2);

  const [replyComments, setReplyComments] = useState([]);

  useEffect(() => {
    const newComment = post.comments?.filter((comment) => !comment.reply);
    setComments(newComment);
    setShowComment(newComment?.slice(0, next));
  }, [next, post.comments]);

  useEffect(() => {
    const newReply = post.comments?.filter((comment) => comment.reply);
    setReplyComments(newReply);
  }, [post?.comments]);

  return (
    <>
      {showComment?.map((comment) => {
        return (
          <CommentItem
            commentId={comment?._id}
            comment={comment}
            post={post}
            key={comment?._id ? comment._id : comment.createdAt}
            replyComment={replyComments?.filter((item) => item.reply === comment._id)}
          />
        );
      })}

      {comments?.length - next > 0 ? (
        <Stack>
          <Typography
            component="span"
            fontSize={minWidth750 ? 14 : 12}
            sx={{
              cursor: "pointer",
              fontWeight: 600,
              "&:hover": {
                textDecoration: "underline",
              },
            }}
            onClick={() => setNext(next + 10)}>
            Xem thêm bình luận
          </Typography>
        </Stack>
      ) : (
        comments?.length > 2 &&
        next >= comments?.length && (
          <Stack>
            <Typography
              component="span"
              sx={{
                cursor: "pointer",
                fontSize: 14,
                fontWeight: 600,
                "&:hover": {
                  textDecoration: "underline",
                },
              }}
              onClick={() => setNext(2)}>
              Ẩn bớt bình luận
            </Typography>
          </Stack>
        )
      )}
    </>
  );
};

export default Comments;
