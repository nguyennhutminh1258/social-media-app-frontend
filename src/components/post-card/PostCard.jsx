import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";
import ModeCommentOutlinedIcon from "@mui/icons-material/ModeCommentOutlined";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import {
  Avatar,
  Box,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  IconButton,
  Menu,
  MenuItem,
  Typography,
} from "@mui/material";
import { Stack } from "@mui/system";
import moment from "moment";
import "moment/locale/vi";
import { memo, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import {
  deletePost,
  likePost,
  setOnEdit,
  unLikePost,
} from "../../store/slice/postSlice";
import { deleteUserPost } from "../../store/slice/profileSlice";
import { setPostEdit } from "../../store/slice/statusSlice";
import Comments from "../comments/Comments";
import ConfirmDialog from "../confirm-dialog/ConfirmDialog";
import ImageCarosel from "../image-carosel/ImageCarosel";
import InputComment from "../input-comment/InputComment";
import LikeButton from "../like-button/LikeButton";

const PostCard = ({ post }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { content, images, likes, comments } = post;
  const { _id, avatar, userName } = post.user;

  const userInfo = useSelector((state) => state.user.userInfo);
  const token = useSelector((state) => state.user.token);
  const socket = useSelector((state) => state.socket);

  // Hiện thêm / Ẩn bớt nội dung status
  const [readMore, setReadMore] = useState(false);
  // Thích bài viết
  const [liked, setLiked] = useState(false);
  // Hiện menu sửa, xóa bài viết
  const [anchorElMenu, setAnchorElMenu] = useState(null);
  const open = Boolean(anchorElMenu);
  // Hiện dialog xác nhận xóa bài viết
  const [openConfirm, setOpenConfirm] = useState(false);
  const [inView, setInView] = useState(true);
  const postRef = useRef();

  const handleOpenConfirm = () => {
    setOpenConfirm(true);
  };

  const handleCloseConfirm = () => {
    setOpenConfirm(false);
  };

  const handleClick = (event) => {
    setAnchorElMenu(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorElMenu(null);
  };

  const handleReadMore = () => {
    setReadMore(!readMore);
  };

  const handleEditPost = () => {
    dispatch(setPostEdit(post));
    dispatch(setOnEdit(true));
    handleClose();
  };

  const handleLikePost = () => {
    if (token) {
      setLiked(true);
      dispatch(likePost({ post, userInfo, socket }));
    } else {
      navigate("/login");
    }
  };

  const handleUnlikePost = () => {
    setLiked(false);
    dispatch(unLikePost({ post, userInfo, socket }));
  };

  const handleDeletePost = () => {
    dispatch(deletePost(post));
    dispatch(deleteUserPost(post));
  };

  const handleCopyLink = () => {
    navigator.clipboard.writeText(
      `https://social-media-app-0ltx.onrender.com/post/${post._id}`
    );
    handleClose();
  };

  useEffect(() => {
    if (post.likes.find((like) => like._id === userInfo._id)) {
      setLiked(true);
    }
  }, [post.likes, userInfo._id]);

  useEffect(() => {
    let callback = (entries) => {
      entries.forEach((entry) => {
        setInView(entry.isIntersecting);
      });
    };
    let observer = new IntersectionObserver(callback);

    if (postRef?.current) {
      observer.observe(postRef.current);
    }

    return () => {
      observer.disconnect();
    };
  }, [postRef]);

  return (
    <>
      <Box sx={{ width: "100%", marginBottom: 2 }} ref={postRef}>
        {inView ? 
           <Card sx={{ width: "100%"}}>
            <CardHeader
              avatar={<Avatar src={avatar} />}
              action={
                <IconButton aria-label="settings" onClick={handleClick}>
                  <MoreVertIcon />
                </IconButton>
              }
              title={
                <Typography component="h4" fontWeight={700}>
                  <Link
                    to={`/profile/${_id}`}
                    style={{ textDecoration: "none", color: "inherit" }}
                  >
                    {userName}
                  </Link>
                </Typography>
                }
              subheader={moment(post.createdAt).fromNow()}
            />
            <CardContent>
              <Typography
                variant="body2"
                color="text.secondary"
                fontWeight={500}
                whiteSpace="pre-line"
              >
                {content.length < 60
                  ? content
                  : readMore
                  ? content + " "
                  : content.slice(0, 60) + "..."}{" "}
                {content.length > 60 && (
                  <Typography
                    component="span"
                    fontSize={15}
                    fontWeight={700}
                    sx={{
                      cursor: "pointer",
                      "&:hover": {
                        textDecoration: "underline",
                      },
                    }}
                    onClick={handleReadMore}
                  >
                    {readMore ? "Ẩn bớt" : "Xem thêm"}
                  </Typography>
                )}
              </Typography>
            </CardContent>
  
            <ImageCarosel images={images} />
 
            <CardActions>
              <Stack sx={{ width: "100%" }}>
                <Stack direction="row">
                  <LikeButton
                    liked={liked}
                    handleLike={handleLikePost}
                    handleUnlike={handleUnlikePost}
                  />
                  <IconButton
                    aria-label="comment"
                    size="large"
                    onClick={() => navigate(`/post/${post._id}`)}
                  >
                    <ModeCommentOutlinedIcon />
                  </IconButton>
                </Stack>
                  <Stack direction="row" justifyContent="space-between" px={2}>
                    <Typography
                      component="span"
                      sx={{ fontSize: 14, fontWeight: 700, lineHeight: 1 }}
                    >
                      {likes.length} lượt thích
                    </Typography>
                    <Typography
                      component="span"
                      sx={{ fontSize: 14, fontWeight: 700, lineHeight: 1}}
                    >
                      {comments.length} bình luận
                    </Typography>
                  </Stack>
                </Stack>
            </CardActions>
            <CardActions>
              <Stack sx={{ width: "100%" }}>
                <InputComment sx={{ paddingLeft: "10px" }} post={post} />
                <Divider sx={{ margin: "10px 0" }} />
                <Comments post={post} />
              </Stack>
            </CardActions>
          </Card> 
          : 
          <></>
        }
      </Box>
      <Menu
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        anchorEl={anchorElMenu}
        open={open}
        onClose={handleClose}
      >
        {userInfo._id === _id ? (
          <Box>
            <MenuItem onClick={handleEditPost} disableRipple>
              <EditIcon sx={{ marginRight: 1.5 }} fontSize="small" />
              <Typography component="span" fontSize={15}>
                Sửa bài viết
              </Typography>
            </MenuItem>
            <Divider sx={{ my: 0.5 }} />
            <MenuItem onClick={handleOpenConfirm} disableRipple>
              <DeleteIcon sx={{ marginRight: 1.5 }} fontSize="small" />
              <Typography component="span" fontSize={15}>
                Xóa bài viết
              </Typography>
            </MenuItem>
            <Divider sx={{ my: 0.5 }} />
            <MenuItem onClick={handleCopyLink} disableRipple>
              <ContentCopyIcon sx={{ marginRight: 1.5 }} fontSize="small" />
              <Typography component="span" fontSize={15}>
                Sao chép liên kết
              </Typography>
            </MenuItem>
          </Box>
        ) : (
          <MenuItem onClick={handleCopyLink} disableRipple>
            <ContentCopyIcon sx={{ marginRight: 1.5 }} fontSize="small" />
            <Typography component="span" fontSize={15}>
              Sao chép liên kết
            </Typography>
          </MenuItem>
        )}
      </Menu>
      <ConfirmDialog
        openConfirm={openConfirm}
        handleCloseConfirm={handleCloseConfirm}
        handleDeletePost={handleDeletePost}
      />
    </>
  );
};

export default memo(PostCard);
