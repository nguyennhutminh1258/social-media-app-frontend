import { AppBar, Box, Button, Stack, Toolbar, Typography } from "@mui/material";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import SearchBox from "../search-box/SearchBox";
import Logo from "../../assets/image/logo.png";
import { useSelector } from "react-redux";

const Header = () => {
  const navigate = useNavigate();
  const [search, setSearch] = useState("");

  const token = useSelector((state) => state.user.token);

  return (
    <Box sx={{ flexGrow: 1, position: "sticky", top: 0, zIndex: 1000 }}>
      <AppBar position="relative">
        <Toolbar>
            <Stack
              width="100%"
              direction="row"
              spacing={3}
              alignItems="center"
              justifyContent="space-between">
              <Stack direction="row" spacing={1} alignItems="center">
                <Link to="/">
                  <Box>
                    <img src={Logo} width={25} height={25} alt="music-icon" />
                  </Box>
                </Link>
                <Box
                  sx={{
                    "@media (max-width: 500px)": {
                      display: "none",
                    },
                  }}>
                  <Link
                    to="/"
                    style={{
                      textDecoration: "none",
                      color: "white",
                    }}>
                    <Typography variant="h6" noWrap component="h3" sx={{ marginRight: "16px", fontSize: 16 }}>
                      SOCIAL MEDIA APP
                    </Typography>
                  </Link>
                </Box>
              </Stack>

              {token ? (
                <Box sx={{ maxWidth: 500, width: "100%" }}>
                  <SearchBox search={search} setSearch={setSearch} />
                </Box>
              ) : (
                <Stack width="100%" direction="row" spacing={1} alignItems="center" justifyContent="flex-end">
                  <Button
                    type="submit"
                    variant="contained"
                    color="warning"
                    size="small"
                    onClick={() => navigate("/login")}
                    sx={{ borderRadius: 20 }}>
                    Đăng nhập
                  </Button>
                </Stack>
              )}
            </Stack>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Header;
