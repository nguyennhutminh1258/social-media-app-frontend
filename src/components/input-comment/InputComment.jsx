import { Avatar, IconButton, InputBase, Stack } from "@mui/material";
import { styled } from "@mui/material/styles";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { createComment } from "../../store/slice/commentSlice";
import SendOutlinedIcon from "@mui/icons-material/SendOutlined";
import SentimentVerySatisfiedIcon from "@mui/icons-material/SentimentVerySatisfied";
import EmojiMenu from "../emoji-menu/EmojiMenu";
import { useState } from "react";

const Input = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  width: "100%",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 1.5),
    width: "100%",
    fontSize: "13px",
    "&::placeholder": {
      fontSize: "13px",
    },
  },
}));

const InputComment = ({ post }) => {
  const dispatch = useDispatch();

  const { register, handleSubmit, reset, setValue, getValues } = useForm();

  const userInfo = useSelector((state) => state.user.userInfo);
  //Socket
  const socket = useSelector((state) => state.socket);

  // Hiện menu emoji
  const [anchorEmoji, setAnchorEmoji] = useState(null);
  const openEmoji = Boolean(anchorEmoji);

  const handleOpenEmoji = (event) => {
    setAnchorEmoji(event.currentTarget);
  };
  const handleCloseEmoji = () => {
    setAnchorEmoji(null);
  };

  const handlePostComment = (data) => {
    const newComment = {
      content: data.content,
      likes: [],
      user: userInfo,
      createdAt: new Date().toISOString(),
    };
    dispatch(createComment({ post, newComment, socket }));
    reset();
  };

  return (
    <>
      <form onSubmit={handleSubmit(handlePostComment)}>
        <Stack width="100%" direction="row" spacing={2}>
          <Avatar src={userInfo.avatar} />
          <Stack width="100%" direction="row" alignItems="center">
            <Input
              {...register("content", {
                required: true,
              })}
              multiline={true}
              placeholder="Thêm bình luận..."
              size="small"
              fullWidth
            />
            <IconButton onClick={handleOpenEmoji}>
              <SentimentVerySatisfiedIcon />
            </IconButton>
            <IconButton type="submit">
              <SendOutlinedIcon />
            </IconButton>
          </Stack>
        </Stack>
      </form>
      <EmojiMenu
        anchorEmoji={anchorEmoji}
        openEmoji={openEmoji}
        handleCloseEmoji={handleCloseEmoji}
        setValue={setValue}
        getValues={getValues}
      />
    </>
  );
};

export default InputComment;
