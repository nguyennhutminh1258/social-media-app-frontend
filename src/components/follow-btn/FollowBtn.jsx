import { Button } from "@mui/material";
import { Box } from "@mui/system";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { follow, unfollow } from "../../store/slice/profileSlice";

const FollowBtn = ({ userProfile }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const token = useSelector((state) => state.user.token);
  const userInfo = useSelector((state) => state.user.userInfo);

  const [followed, setFollowed] = useState(false);

  const handleFollow = () => {
    if (token) {
      setFollowed(true);
      dispatch(follow({ idUser: userProfile?._id, idUserFollower: userInfo?._id }));
    } else {
      navigate("/login");
    }
  };

  const handleUnFollow = () => {
    if (token) {
      setFollowed(false);
      dispatch(unfollow({ idUser: userProfile?._id, idUserFollower: userInfo?._id }));
    } else {
      navigate("/login");
    }
  };

  useEffect(() => {
    if (userInfo.following?.some((item) => item._id === userProfile?._id)) {
      setFollowed(true);
    }
  }, [userInfo?.following, userProfile?._id]);

  return (
    <>
      {followed ? (
        <Box>
          <Button
            color="error"
            size="small"
            sx={{ fontSize: 12, borderRadius: 20, minWidth: 110 }}
            variant="outlined"
            onClick={handleUnFollow}>
            Bỏ theo dõi
          </Button>
        </Box>
      ) : (
        <Box>
          <Button
            color="secondary"
            size="small"
            sx={{ fontSize: 12, borderRadius: 20, minWidth: 110 }}
            variant="outlined"
            onClick={handleFollow}>
            Theo dõi
          </Button>
        </Box>
      )}
    </>
  );
};

export default FollowBtn;
